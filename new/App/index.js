import React from 'react';
import { AppRegistry, Image } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator, createMaterialTopTabNavigator } from 'react-navigation-tabs';
// import App from './App';
import { name as appName } from './app.json';
//import Main from './Component/MainComponent';
import Home from './component/homeComponent';
import User from './component/userComponent';
import Login from './component/loginComponent';
import Register from './component/registerComponent';
import ForgotPass from './component/forgotPassComponent';
import ChangePass from './component/changePassComponent';
import Filter from './component/filterComponent';

const tabNavigator = createMaterialTopTabNavigator(
    {
        Muabán: {
            screen: Home,
            navigationOptions: {
                tabBarIcon: () => (<Image
                    source={require('./icon/home.png')}
                    style={{
                        width: 20,
                        height: 20,
                        tintColor: 'black',
                        fontWeight: "bold"
                    }}></Image>)
            }
        },
        Củatôi: {
            screen: Login,
            navigationOptions: {
                tabBarIcon: () => (<Image
                    source={require('./icon/topic.png')}
                    style={{
                        width: 20,
                        height: 20,
                        tintColor: 'black',
                        fontWeight: "bold"
                    }}></Image>)
            }
        },
        Đăngtin: {
            screen: Login,
            navigationOptions: {
                tabBarIcon: () => (<Image
                    source={require('./icon/new.png')}
                    style={{
                        width: 20,
                        height: 20,
                        tintColor: 'black',
                        fontWeight: "bold"
                    }}></Image>)
            }
        },
        Cánhân: {
            screen: User,
            navigationOptions: {
                tabBarIcon: () => (<Image
                    source={require('./icon/user.png')}
                    style={{
                        width: 20,
                        height: 20,
                        tintColor: 'black',
                        fontWeight: "bold"
                    }}></Image>)
            }
        },
    },
    {
        // order: ['Home', 'LoveSongs', 'Personal'],
        initialRouteName: 'Muabán',
        animationEnabled: true,
        tabBarPosition: 'bottom',
        swipeEnabled: true,
        tabBarOptions: {
            showIcon: true,
            activeTintColor: '#FF6600',
            style: { backgroundColor: '#DDDDDD' },
            labelStyle: { color: 'black' },
            //    activeBackgroundColor: 'rgb(93,153,203)'
            // activeBackgroundColor: 'rgb(237, 255, 255)'
        }
    },
);

const stackMain = createStackNavigator(
    {
        Muabán: {
            screen: tabNavigator,
        },
        Login: {
            screen: Login,
        },
        ForgotPass: {
            screen: ForgotPass,
        },
        Register: {
            screen: Register,
        },
        // View: {
        //     screen: ViewComponent,
        // },
        ChangePass: {
            screen: ChangePass,
        }
    },
    {
        // initialRouteName: 'Login',
        // headerMode: 'none',
        initialRouteName: 'Muabán',
        headerMode: 'none',
    },
);

AppRegistry.registerComponent(appName, () => createAppContainer(stackMain));
