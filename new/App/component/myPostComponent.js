import React, { Component } from 'react';
import { Image, View, Text, FlatList, Alert } from 'react-native';
import callApi from '../util/apiCaller';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import Swipeout from 'react-native-swipeout';
import AddModal from './editPost';
const uri = "http://192.168.56.1:1998/image/upload/";

export let postId = ''

class FlatListItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            deletedRowKey: null,
            activeRowKey: null,
        }
    }
    deletePost = async () => {
        const id = this.props.item._id
        console.log("========================================>", id);
        await callApi('DELETE', 'http://192.168.56.1:1998/api/deletePost', {
            "id": id
        }).then(res => {
            if (res.data.error === false) {
                alert("xoa thành công");
                this.props.parentFlatList.refreshFlatList(id)
            } else {
                alert("loi loi loi !")
            }
        }).catch(err => {
            console.log("fail 222", err);
        })
    }
    render() {
        // const { title } = this.props.item
        // console.log("title   ", title);
        // const { address } = this.props.item
        // console.log("address   ", address);
        // console.log('duc 22', _id)
        const { _id } = this.props.item
        const SwipeSeting = {
            autoClose: true,
            onClose: (selectId, rowId, direction) => {
                if (this.state.activeRowKey != null) {
                    this.setState({ activeRowKey: null })
                }
            },
            onOpen: (selectId, rowId, direction) => {
                this.setState({ activeRowKey: this.props.item._id },
                    () => {
                        console.log(this.state.activeRowKey)
                    })
            },
            right: [
                {
                    onPress: () => {
                        postId = _id,
                            this.props.parentFlatList.refs.addModal.showAddModal();
                    },
                    text: 'Sửa', type: 'primary'
                },
                {
                    onPress: () => {
                        // const deletetingRow = this.state.activeRowKey
                        Alert.alert(
                            'Alert',
                            'Bạn có chắc chắn muốn xóa ?',
                            [
                                { text: "No", onPress: () => console.log('Không xóa'), style: 'cancel' },
                                {
                                    text: 'Yes', onPress: async () => {
                                        this.deletePost();
                                    }
                                }
                            ],
                            { cancelable: true }
                        );
                    },
                    text: 'Xóa', type: 'delete'
                }
            ],
            rowId: this.props.index,
            sectionId: 1
        };
        return (
            <Swipeout {...SwipeSeting}>
                <View
                    style={{
                        margin: 10,
                        flex: 1,
                        // flexDirection: 'column',
                        // justifyContent: 'flex-start',
                        borderTopLeftRadius: 20,
                        borderTopRightRadius: 20,
                        borderBottomLeftRadius: 20,
                        borderBottomRightRadius: 20,
                        borderWidth: 1

                    }}>
                    <View style={{ flex: 50, }}>
                        <Image style={{
                            borderTopLeftRadius: 20,
                            borderTopRightRadius: 20,
                            width: 347,
                            height: 200,
                        }}
                            source={{
                                uri: uri + this.props.item.image
                            }}
                        />
                    </View>
                    <View style={{ flex: 50 }}>
                        <View style={{
                            flexDirection: 'column',
                            justifyContent: 'flex-start',
                            marginTop: 10
                        }}>
                            <View>
                                <Text style={{ fontWeight: "bold", fontSize: 19, marginLeft: 5 }}> {this.props.item.title}</Text>
                            </View>
                            <View>
                                <Text style={{ fontSize: 14, marginLeft: 5 }}> {this.props.item.address}</Text>
                            </View>
                        </View>
                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'flex-start'
                        }}>
                            <View style={{ flex: 50, flexDirection: 'row', backgroundColor: 'yellow' }}>
                                <View style={{ fontSize: 5, marginLeft: 5 }}>
                                    <Image source={require("../icon/money.png")}>
                                    </Image>
                                </View>
                                <View style={{ fontSize: 5, }}>
                                    <Text style={{ fontWeight: "bold", marginTop: 20 }}></Text>
                                </View>
                            </View>
                            <View style={{ flex: 50, flexDirection: 'row' }}>
                                <View style={{ fontSize: 5, marginLeft: 5 }}>
                                    <Image source={require("../icon/people.png")}>
                                    </Image>
                                </View>
                                <View style={{ fontSize: 5, marginLeft: 5 }}>
                                    <Text style={{ fontWeight: "bold", marginTop: 25 }}>{this.props.item.idUser.name}</Text>
                                </View>
                            </View>
                        </View>

                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'flex-start'
                        }}>
                            <View style={{ flex: 50, flexDirection: 'row', }}>
                                <View style={{ fontSize: 5, marginLeft: 5 }}>
                                    <Image source={require("../icon/telephone.png")}>
                                    </Image>
                                </View>
                                <View style={{ fontSize: 5, marginLeft: 5 }}>
                                    <Text style={{ fontWeight: "bold", marginTop: 25 }}>{this.props.item.idUser.phone}</Text>
                                </View>
                            </View>
                            <View style={{ flex: 50, flexDirection: 'row' }}>
                                <View style={{ fontSize: 5, marginLeft: 5 }}>
                                    <Image source={require("../icon/like.png")}>
                                    </Image>
                                </View>
                                <View style={{ fontSize: 5, marginLeft: 5 }}>
                                    <Text style={{ fontWeight: "bold", marginTop: 20 }}>yêu thích</Text>
                                </View>
                            </View>
                        </View>
                        <View>
                            <TouchableOpacity
                                onPress={() => {
                                    console.log('duc 4444', this.props.navigation)
                                    this.props.navigation.navigate('Detail', { idPost: _id })
                                }}
                            >
                                <Text style={{ color: "#FF6600", marginLeft: 135, fontWeight: "bold" }}>Xem chi tiết</Text>
                            </TouchableOpacity>

                        </View>
                    </View>

                </View>
            </Swipeout>
        );
    }
}
export default class myPost extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            refreshing: false,
            deletedRowKey: null,
        };
        this.flatList();
        //      this._onPressAdd = this._onPressAdd.bind(this);
    }

    refreshFlatList = (deletedKey) => {
        const newData = this.state.data.filter((data, index) => data._id != deletedKey)
        this.setState({ data: newData })
    }

    // _onPressAdd() {
    //     this.refs.AddModal.showAddModal();
    // }

    flatList = async () => {
        // nho bo cmt ra khi co dang nhap 
        const email = this.props.navigation.state.params.email
        console.log("========================================>", email);
        await callApi('POST', 'http://192.168.56.1:1998/api/getMyPost', {
            "email": email
        }).then(res => {
            if (res.data.error === false) {
                this.setState({
                    data: res.data.data
                }, () => console.log("data tra ve: ", this.state.data))
            } else {
                alert("loi loi loi !")
            }
        }).catch(err => {
            console.log("fail 222", err);
        })
    }

    updatedata = async (data) => {
        console.log("----------> ", data);
        const newData = this.state.data.map((item, index) => {
            if (item._id == data.data._id) {
                item.title = data.data.title
                item.address = data.data.address
            }
            return item
        })

        this.setState({ data: newData })
    }

    render() {
        // nho bo cmt ra khi co dang nhap 
        const email = this.props.navigation.state.params.email
        return (
            <View style={{ flex: 1 }}>
                <View style={{ height: 35, backgroundColor: "#FF6600" }}>
                    <View>
                        <Text style={{
                            color: 'white',
                            fontWeight: 'bold',
                            fontSize: 25,
                            marginTop: 6,
                            marginLeft: 130
                        }}>
                            Bản Tin
                        </Text>
                    </View>
                </View>
                <View style={{ height: 30 }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ marginLeft: 20 }}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.props.navigation.navigate('myPostNoActive', { email: email })
                                }}
                            >
                                <Image source={require("../icon/actention.png")}

                                >
                                </Image>
                            </TouchableOpacity>

                        </View>
                        <View style={{ marginLeft: 260 }}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.props.navigation.navigate('ListLike', { email: email })
                                }}>
                                <Image style={{ width: 30, height: 30, }} source={require("../icon/save.png")}>
                                </Image>
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>
                <View style={{ flex: 1, backgroundColor: "white" }}>
                    <View>
                        <FlatList
                            data={this.state.data}
                            renderItem={({ item, index }) => {
                                console.log(`Item = ${JSON.stringify(item)}, Index = ${index}`);
                                return (<FlatListItem item={item} index={index} parentFlatList={this} navigation={this.props.navigation} >

                                </FlatListItem>)
                            }}
                        >
                        </FlatList>
                    </View>
                    <AddModal ref={'addModal'} updatedata={this.updatedata} parentFlatList={this}>

                    </AddModal>
                </View>

            </View>
        );
    }
}
