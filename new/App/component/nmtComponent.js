import React, { Component } from 'react';
import { Image, View, Text, FlatList } from 'react-native';
import callApi from '../util/apiCaller';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { gmail } from './loginComponent';
const uri = "http://192.168.56.1:1998/image/upload/";

class FlatListItem extends Component {
    render() {
        const { _id } = this.props.item

        console.log('duc 22', _id)
        return (
            <View
                style={{
                    margin: 10,
                    flex: 1,
                    // flexDirection: 'column',
                    // justifyContent: 'flex-start',
                    borderTopLeftRadius: 20,
                    borderTopRightRadius: 20,
                    borderBottomLeftRadius: 20,
                    borderBottomRightRadius: 20,
                    borderWidth: 1

                }}>
                <View style={{ flex: 50, }}>
                    <Image style={{
                        borderTopLeftRadius: 20,
                        borderTopRightRadius: 20,
                        width: 347,
                        height: 200,
                    }}
                        source={{
                            uri: uri + this.props.item.image
                        }}
                    />
                </View>
                <View style={{ flex: 50 }}>
                    <View style={{
                        flexDirection: 'column',
                        justifyContent: 'flex-start',
                        marginTop: 10
                    }}>
                        <View>
                            <Text style={{ fontWeight: "bold", fontSize: 19, marginLeft: 5 }}> {this.props.item.title}</Text>
                        </View>
                        <View>
                            <Text style={{ fontSize: 14, marginLeft: 5 }}> {this.props.item.address}</Text>
                        </View>
                    </View>
                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'flex-start'
                    }}>
                        <View style={{ flex: 50, flexDirection: 'row', backgroundColor: 'yellow' }}>
                            <View style={{ fontSize: 5, marginLeft: 5 }}>
                                <Image source={require("../icon/money.png")}>
                                </Image>
                            </View>
                            <View style={{ fontSize: 5, }}>
                                <Text style={{ fontWeight: "bold", marginTop: 20 }}>{this.props.item.minPrice}</Text>
                            </View>
                        </View>
                        <View style={{ flex: 50, flexDirection: 'row' }}>
                            <View style={{ fontSize: 5, marginLeft: 5 }}>
                                <Image source={require("../icon/people.png")}>
                                </Image>
                            </View>
                            <View style={{ fontSize: 5, marginLeft: 5 }}>
                                <Text style={{ fontWeight: "bold", marginTop: 25 }}>{this.props.item.idUser.name}</Text>
                            </View>
                        </View>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'flex-start'
                    }}>
                        <View style={{ flex: 50, flexDirection: 'row', }}>
                            <View style={{ fontSize: 5, marginLeft: 5 }}>
                                <Image source={require("../icon/telephone.png")}>
                                </Image>
                            </View>
                            <View style={{ fontSize: 5, marginLeft: 5 }}>
                                <Text style={{ fontWeight: "bold", marginTop: 25 }}>{this.props.item.idUser.phone}</Text>
                            </View>
                        </View>
                        <View style={{ flex: 50, flexDirection: 'row' }}>
                            <View style={{ fontSize: 5, marginLeft: 5 }}>
                                <TouchableOpacity
                                    onPress={async () => {
                                        await callApi('POST', 'http://192.168.56.1:1998/api/createList', {
                                            email: gmail,
                                            idPost: this.props.item._id
                                        })
                                    }}
                                >
                                    <Image source={require("../icon/like.png")}>
                                    </Image>
                                </TouchableOpacity>

                            </View>
                            <View style={{ fontSize: 5, marginLeft: 5 }}>
                                <Text style={{ fontWeight: "bold", marginTop: 20 }}>yêu thích</Text>
                            </View>
                        </View>
                    </View>
                    <View>
                        <TouchableOpacity
                            onPress={() => {
                                console.log('duc 4444', this.props.navigation)
                                this.props.navigation.navigate('Detail1', { idPost: _id })
                            }}
                        >
                            <Text style={{ color: "#FF6600", marginLeft: 135, fontWeight: "bold" }}>Xem chi tiết</Text>
                        </TouchableOpacity>

                    </View>
                </View>

            </View>
        );
    }
}
export default class NMT extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFetching: false,
            data: [],
            refreshing: false
        };
        this.flatList();
    }
    flatList = async () => {
        console.log("success 000");
        let { } = this.state;
        this.setState({ isFetching: true })
        await callApi('GET', 'http://192.168.56.1:1998/api/getPostMT', {

        }).then(res => {
            if (res.data.error === false) {
                this.setState({
                    data: res.data.data,
                    isFetching: false
                }, () => console.log("data tra ve: ", this.state.data))
            } else {
                alert("loi loi loi !")
            }
        }).catch(err => {
            console.log("fail 222", err);
        })
    }
    render() {
        console.log('duc 00', this.props)
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 7, backgroundColor: "#FF6600", flexDirection: 'row' }}>
                    <View>
                        <TouchableOpacity
                            onPress={() => {
                                this.props.navigation.navigate('Muabán')
                            }}
                        >
                            <Image style={{ height: 30, width: 30, marginTop: 10 }} source={require("../icon/back.png")}>
                            </Image>
                        </TouchableOpacity>

                    </View>
                    <View>
                        <Text style={{
                            color: 'white',
                            fontWeight: 'bold',
                            fontSize: 25,
                            marginTop: 6,
                            marginLeft: 130
                        }}>
                            Bản Tin
                        </Text>
                    </View>
                </View>
                <View style={{ flex: 93, backgroundColor: "white" }}>
                    <FlatList
                        onRefresh={() => this.flatList()}
                        refreshing={this.state.isFetching}
                        data={this.state.data}
                        renderItem={({ item, index }) => {
                            console.log(`Item = ${JSON.stringify(item)}, Index = ${index}`);
                            return (<FlatListItem item={item} index={index} navigation={this.props.navigation} >

                            </FlatListItem>)
                        }}
                    >

                    </FlatList>
                </View>
            </View>
        );
    }
}
