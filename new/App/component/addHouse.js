import React, { Component } from 'react';
import {
    AppRegistry, FlatList, Text, StyleSheet, View, Image, Alert,
    Platform, Dimensions, TextInput
} from 'react-native';
import Modal from 'react-native-modalbox';
import Button from 'react-native-button';
import { id } from './createHouseComponent';
import callApi from '../util/apiCaller';
import { log } from 'react-native-reanimated';
var screen = Dimensions.get('window');
export default class CreateModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tennha: "",
            phaply: "",
            dientich: "",
            gia: "",
            lau: "",
            bancong: "",
            phongngu: "",
            phongtam: "",
            santhuong: "",
            sanvuon: "",
        }
    }
    showAddModal1 = () => {
        this.refs.myModal1.open()
    }
    render() {
        return (
            <Modal
                ref={"myModal1"}
                style={{
                    justifyContent: 'center',
                    borderRadius: 13,
                    shadowRadius: 10,
                    width: screen.width - 60,
                    height: 450
                }}
                position='center'
                backdrop={true}
                onClosed={() => {

                }}
            >
                <Text style={{
                    fontSize: 16,
                    fontWeight: 'bold',
                    textAlign: 'center',
                }}
                >Thêm nhà</Text>

                <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginTop: 13, marginLeft: 5, }}>
                        <Image style={{ height: 30, width: 30 }} source={require("../icon/nha.png")}></Image>
                    </View>
                    <View >
                        <TextInput
                            style={{
                                height: 40,
                                borderBottomColor: 'gray',
                                marginLeft: 30,
                                marginRight: 30,
                                marginTop: 10,
                                marginBottom: 10,
                                borderBottomWidth: 1
                            }}
                            onChangeText={(tennha) => this.setState({ tennha: tennha })}
                            placeholder="tên nhà                                        "
                            value={this.state.tennha}
                        />
                    </View>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginTop: 13, marginLeft: 5 }}>
                        <Image style={{ height: 30, width: 30 }} source={require("../icon/phaply.png")}></Image>
                    </View>
                    <View >
                        <TextInput
                            style={{
                                height: 40,
                                borderBottomColor: 'gray',
                                marginLeft: 30,
                                marginRight: 30,
                                marginTop: 10,
                                marginBottom: 10,
                                borderBottomWidth: 1
                            }}
                            onChangeText={(phaply) => this.setState({ phaply: phaply })}
                            placeholder="pháp lý                                       "
                            value={this.state.phaply}
                        />
                    </View>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ marginTop: 13, }}>
                            <Image style={{ height: 30, width: 30 }} source={require("../icon/dientich.png")}></Image>
                        </View>
                        <TextInput
                            style={{
                                height: 40,
                                borderBottomColor: 'gray',
                                marginLeft: 10,
                                marginRight: 30,
                                marginTop: 10,
                                marginBottom: 10,
                                borderBottomWidth: 1
                            }}
                            keyboardType="numeric"
                            onChangeText={(dientich) => this.setState({ dientich: dientich })}
                            placeholder="diện tích    "
                            value={this.state.dientich}
                        />
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ marginTop: 13, }}>
                            <Image style={{ height: 30, width: 30 }} source={require("../icon/gia.png")}></Image>
                        </View>
                        <TextInput
                            style={{
                                height: 40,
                                borderBottomColor: 'gray',
                                marginLeft: 10,
                                marginRight: 30,
                                marginTop: 10,
                                marginBottom: 10,
                                borderBottomWidth: 1
                            }}
                            keyboardType="numeric"
                            onChangeText={(gia) => this.setState({ gia: gia })}
                            placeholder="giá           "
                            value={this.state.gia}
                        />
                    </View>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ marginTop: 13, }}>
                            <Image style={{ height: 30, width: 30 }} source={require("../icon/lau.png")}></Image>
                        </View>
                        <TextInput
                            style={{
                                height: 40,
                                borderBottomColor: 'gray',
                                marginLeft: 10,
                                marginRight: 30,
                                marginTop: 10,
                                marginBottom: 10,
                                borderBottomWidth: 1
                            }}
                            keyboardType="numeric"
                            onChangeText={(lau) => this.setState({ lau: lau })}
                            placeholder="lầu            "
                            value={this.state.lau}
                        />
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ marginTop: 13, }}>
                            <Image style={{ height: 30, width: 30 }} source={require("../icon/bancong.png")}></Image>
                        </View>
                        <TextInput
                            style={{
                                height: 40,
                                borderBottomColor: 'gray',
                                marginLeft: 10,
                                marginRight: 30,
                                marginTop: 10,
                                marginBottom: 10,
                                borderBottomWidth: 1
                            }}
                            keyboardType="numeric"
                            onChangeText={(bancong) => this.setState({ bancong: bancong })}
                            placeholder="ban công"
                            value={this.state.bancong}
                        />
                    </View>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ marginTop: 13, }}>
                            <Image style={{ height: 30, width: 30 }} source={require("../icon/phongngu.png")}></Image>
                        </View>
                        <TextInput
                            style={{
                                height: 40,
                                borderBottomColor: 'gray',
                                marginLeft: 10,
                                marginRight: 30,
                                marginTop: 10,
                                marginBottom: 10,
                                borderBottomWidth: 1
                            }}
                            keyboardType="numeric"
                            onChangeText={(phongngu) => this.setState({ phongngu: phongngu })}
                            placeholder="phòng ngủ"
                            value={this.state.phongngu}
                        />
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ marginTop: 13, }}>
                            <Image style={{ height: 30, width: 30 }} source={require("../icon/phongtam.png")}></Image>
                        </View>
                        <TextInput
                            style={{
                                height: 40,
                                borderBottomColor: 'gray',
                                marginLeft: 10,
                                marginRight: 30,
                                marginTop: 10,
                                marginBottom: 10,
                                borderBottomWidth: 1
                            }}
                            keyboardType="numeric"
                            onChangeText={(phongtam) => this.setState({ phongtam: phongtam })}
                            placeholder="phòng tắm"
                            value={this.state.phongtam}
                        />
                    </View>
                </View>

                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ marginTop: 13, }}>
                            <Image style={{ height: 30, width: 30 }} source={require("../icon/santhuong.png")}></Image>
                        </View>
                        <TextInput
                            style={{
                                height: 40,
                                borderBottomColor: 'gray',
                                marginLeft: 10,
                                marginRight: 30,
                                marginTop: 10,
                                marginBottom: 10,
                                borderBottomWidth: 1
                            }}
                            keyboardType="numeric"
                            onChangeText={(santhuong) => this.setState({ santhuong: santhuong })}
                            placeholder="sân thượng"
                            value={this.state.santhuong}
                        />
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ marginTop: 13 }}>
                            <Image style={{ height: 30, width: 30 }} source={require("../icon/sanvuon.png")}></Image>
                        </View>
                        <TextInput
                            style={{
                                height: 40,
                                borderBottomColor: 'gray',
                                marginLeft: 10,
                                marginRight: 30,
                                marginTop: 10,
                                marginBottom: 10,
                                borderBottomWidth: 1
                            }}
                            keyboardType="numeric"
                            onChangeText={(sanvuon) => this.setState({ sanvuon: sanvuon })}
                            placeholder="sân vườn"
                            value={this.state.sanvuon}
                        />
                    </View>
                </View>

                <Button
                    style={{ fontSize: 18, color: 'white' }}
                    containerStyle={{
                        padding: 8,
                        marginLeft: 70,
                        marginRight: 70,
                        height: 40,
                        borderRadius: 6,
                        backgroundColor: 'mediumseagreen'
                    }}
                    onPress={async () => {
                        console.log("jjjjjjjjjjj ", id)
                        const { tennha, phaply, dientich, lau, gia, bancong, phongngu, phongtam, santhuong, sanvuon } = this.state
                        await callApi('POST', 'http://192.168.56.1:1998/api/createHouse', {
                            note: tennha,
                            legal: phaply,
                            acreage: dientich,
                            price: gia,
                            floor: lau,
                            balcony: bancong,
                            bedroom: phongngu,
                            bathroom: phongtam,
                            terrace: santhuong,
                            placeholder: sanvuon,
                            idPost: id
                        }).then(res => {
                            if (res) {
                                alert("Thêm thành công");
                                this.refs.myModal1.close();
                            }
                        }).catch(err => {
                            console.log("fail 222", err);
                        })

                    }}
                >Lưu</Button>

            </Modal>
        );
    }
}