import React, { Component } from 'react';
import {
    AppRegistry, FlatList, Text, StyleSheet, View, Image, Alert,
    Platform, Dimensions, TextInput
} from 'react-native';
import Modal from 'react-native-modalbox';
import Button from 'react-native-button';
import callApi from '../util/apiCaller';
import { log } from 'react-native-reanimated';
var screen = Dimensions.get('window');
export default class CreateModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            name: "",
            phone: ""
        }
    }
    showAddModal2 = () => {
        this.refs.createModal.open()
    }
    render() {
        return (
            <Modal
                ref={"createModal"}
                style={{
                    justifyContent: 'center',
                    borderRadius: 13,
                    shadowRadius: 10,
                    width: screen.width - 60,
                    height: 350
                }}
                position='center'
                backdrop={true}
                onClosed={() => {

                }}>
                <Text style={{
                    fontSize: 16,
                    fontWeight: 'bold',
                    textAlign: 'center',
                }}
                >Thêm người dùng</Text>

                <View>
                    <TextInput
                        style={{
                            height: 40,
                            borderBottomColor: 'gray',
                            marginLeft: 10,
                            marginRight: 30,
                            marginTop: 10,
                            marginBottom: 10,
                            borderBottomWidth: 1
                        }}
                        onChangeText={(email) => this.setState({ email: email })}
                        placeholder="tên đăng nhập"
                        value={this.state.email}
                    />
                </View>
                <View>
                    <TextInput
                        style={{
                            height: 40,
                            borderBottomColor: 'gray',
                            marginLeft: 10,
                            marginRight: 30,
                            marginTop: 10,
                            marginBottom: 10,
                            borderBottomWidth: 1
                        }}
                        onChangeText={(password) => this.setState({ password: password })}
                        placeholder="mật khẩu"
                        value={this.state.password}
                    />
                </View>
                <View>
                    <TextInput
                        style={{
                            height: 40,
                            borderBottomColor: 'gray',
                            marginLeft: 10,
                            marginRight: 30,
                            marginTop: 10,
                            marginBottom: 10,
                            borderBottomWidth: 1
                        }}
                        onChangeText={(name) => this.setState({ name: name })}
                        placeholder="tên người dùng"
                        value={this.state.name}
                    />
                </View>
                <View>
                    <TextInput
                        style={{
                            height: 40,
                            borderBottomColor: 'gray',
                            marginLeft: 10,
                            marginRight: 30,
                            marginTop: 10,
                            marginBottom: 10,
                            borderBottomWidth: 1
                        }}
                        onChangeText={(phone) => this.setState({ phone: phone })}
                        placeholder="số điện thoại"
                        value={this.state.phone}
                    />
                </View>

                <Button
                    style={{ fontSize: 18, color: 'white' }}
                    containerStyle={{
                        padding: 8,
                        marginLeft: 70,
                        marginRight: 70,
                        height: 40,
                        borderRadius: 6,
                        backgroundColor: 'mediumseagreen'
                    }}
                    onPress={async () => {
                        let { email, password, name, phone } = this.state
                        await callApi('POST', 'http://192.168.56.1:1998/api/createUser', {
                            email: email,
                            password: password,
                            name: name,
                            phone: phone
                        }).then(res => {
                            if (res.data.error === false) {
                                // console.log(res.data.data)
                                // this.props.refeshdata({ data: res.data.data })
                                alert("Thêm thành công");
                                this.refs.createModal.close();
                            }
                        }).catch(err => {
                            console.log("fail 222", err);
                        })

                    }}
                >Lưu</Button>

            </Modal>
        );
    }
}