import React, { Component } from 'react';
import { Image, View, Text, FlatList, Alert } from 'react-native';
import callApi from '../util/apiCaller';
import { TouchableOpacity } from 'react-native-gesture-handler';
import AddModal from './editUser';
const uri = "http://192.168.56.1:1998/image/upload/";

class FlatListItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            deletedRowKey: null,
            activeRowKey: null,
        }
    }

    CapQuyen = async () => {
        const { _id } = this.props.item;
        console.log("tui can iddddd", _id);
        await callApi('PATCH', 'http://192.168.56.1:1998/api/updateUser', {
            "id": _id,
            "idRole": Object("5f49099969d4b2147449d962"),
        }).then(res => {
            if (res.data.err === false) {
                alert("thành công");
            } else {
                alert(" Thất bại");
            }
        }).catch(err => {
            console.log("fail 222", err);
            alert(" Thất bại");
        })
    }

    render() {
        return (
            <View
                style={{
                    margin: 10,
                    flex: 1,
                    borderTopLeftRadius: 20,
                    borderTopRightRadius: 20,
                    borderBottomLeftRadius: 20,
                    borderBottomRightRadius: 20,
                    borderWidth: 1

                }}>

                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View>
                        <Image
                            source={require("../image/2.jpg")}
                            style={{ width: 100, height: 130, borderRadius: 10, marginTop: 10, marginLeft: 10 }}
                        >
                        </Image>
                    </View>

                    <View>
                        <View>
                            <Text style={{ color: 'black', fontWeight: 'bold', marginTop: 15, marginLeft: 50, fontSize: 20 }}>
                                {this.props.item.name}
                            </Text>
                            <Text style={{ color: '#FF6600', marginTop: 5, marginLeft: 20 }}>
                                {this.props.item.email}
                            </Text>
                            <Text style={{ color: '#FF6600', marginTop: 10, marginLeft: 20 }}>
                                {this.props.item.phone}
                            </Text>
                        </View>

                        <View>
                            <TouchableOpacity onPress={() => {
                                this.CapQuyen();
                            }}>
                                <Image style={{ marginLeft: 160 }} source={require("../icon/duyet.png")}>
                                </Image>
                            </TouchableOpacity>
                        </View>
                    </View>


                </View>

            </View>
        );
    }
}
export default class CapQuyen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFetching: false,
            data: [],
            refreshing: false,
            deletedRowKey: null,
        };
        this.flatList();
        // this.createUser = this.createUser.bind(this);
    }

    refreshFlatList = (deletedKey) => {
        const newData = this.state.data.filter((data, index) => data._id != deletedKey)
        this.setState({ data: newData })
    }

    flatList = async () => {
        this.setState({ isFetching: true })
        await callApi('GET', 'http://192.168.56.1:1998/api/getUserNoAdmin', {
            // "email": email
        }).then(res => {
            if (res.data.error === false) {
                this.setState({
                    data: res.data.data,
                    isFetching: false
                }, () => console.log("data tra ve: ", this.state.data))
            } else {
                alert("loi loi loi !")
            }
        }).catch(err => {
            console.log("fail 222", err);
        })
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 7, backgroundColor: "#FF6600", flexDirection: 'row' }}>
                    <View>
                        <TouchableOpacity
                            onPress={() => {
                                this.props.navigation.navigate('Cánhân')
                            }}
                        >
                            <Image style={{ height: 30, width: 30, marginTop: 10 }} source={require("../icon/back.png")}>
                            </Image>
                        </TouchableOpacity>

                    </View>
                    <View>
                        <Text style={{
                            color: 'white',
                            fontWeight: 'bold',
                            fontSize: 25,
                            marginTop: 6,
                            marginLeft: 90
                        }}>
                            Cấp Quyền
                        </Text>
                    </View>
                </View>
                <View style={{ flex: 93, backgroundColor: "white" }}>
                    <FlatList
                        data={this.state.data}
                        renderItem={({ item, index }) => {
                            console.log(`Item = ${JSON.stringify(item)}, Index = ${index}`);
                            return (<FlatListItem item={item} index={index} parentFlatList={this} navigation={this.props.navigation} >

                            </FlatListItem>)
                        }}
                    >
                    </FlatList>
                    <AddModal ref={'addModal'} updatedata={this.updatedata} parentFlatList={this}>

                    </AddModal>
                </View>
            </View>
        );
    }
}
