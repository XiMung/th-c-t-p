import React, { Component } from 'react';
import {
    AppRegistry, FlatList, Text, StyleSheet, View, Image, Alert,
    Platform, Dimensions, TextInput
} from 'react-native';
import Modal from 'react-native-modalbox';
import Button from 'react-native-button';
import { postId } from './quanlyUserComponent';
import callApi from '../util/apiCaller';
import { log } from 'react-native-reanimated';
var screen = Dimensions.get('window');
export default class AddModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            phone: "",
            name: ""
        }
    }
    showAddModal = () => {
        this.refs.myModal.open()
    }
    render() {
        console.log('1111idPostidPostidPost', postId)
        return (
            <Modal
                ref={"myModal"}
                style={{
                    justifyContent: 'center',
                    borderRadius: 13,
                    shadowRadius: 10,
                    width: screen.width - 80,
                    height: 280
                }}
                position='center'
                backdrop={true}
                onClosed={() => {

                }}
            >
                <Text style={{
                    fontSize: 16,
                    fontWeight: 'bold',
                    textAlign: 'center',
                }}
                >Sửa thông tin</Text>
                <TextInput
                    style={{
                        height: 40,
                        borderBottomColor: 'gray',
                        marginLeft: 30,
                        marginRight: 30,
                        marginTop: 20,
                        marginBottom: 10,
                        borderBottomWidth: 1
                    }}
                    onChangeText={(name) => this.setState({ name: name })}
                    placeholder="Nhập tên mới của bạn"
                    value={this.state.name}
                />
                <TextInput
                    style={{
                        height: 40,
                        borderBottomColor: 'gray',
                        marginLeft: 30,
                        marginRight: 30,
                        marginTop: 20,
                        marginBottom: 10,
                        borderBottomWidth: 1
                    }}
                    onChangeText={(phone) => this.setState({ phone: phone })}
                    placeholder="Nhập số điện thoại"
                    value={this.state.phone}
                />

                <Button
                    style={{ fontSize: 18, color: 'white' }}
                    containerStyle={{
                        padding: 8,
                        marginLeft: 70,
                        marginRight: 70,
                        height: 40,
                        borderRadius: 6,
                        backgroundColor: 'mediumseagreen'
                    }}
                    onPress={async () => {
                        const { phone, name } = this.state

                        console.log("hieu1111 ", postId);
                        await callApi('PATCH', 'http://192.168.56.1:1998/api/updateUser', {
                            "id": postId,
                            "phone": phone,
                            "name": name
                        }).then(res => {
                            if (res.data.err === false) {
                                this.props.updatedata({ data: res.data.data })
                                alert("Cập nhật thành công");
                                this.refs.myModal.close();
                            }
                        }).catch(err => {
                            console.log("fail 222", err);
                        })
                        // this.state.FlatListItem.refreshFlatListItem()

                    }}
                >Lưu</Button>

            </Modal>
        );
    }
}