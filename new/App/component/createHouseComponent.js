import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import callApi from '../util/apiCaller';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Button } from 'native-base';
import { TextInput } from 'react-native-gesture-handler';
import CreateModal from './addHouse';
export let id = '';

export default class CreateHouse extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
        this.created = this.created.bind(this);
    }

    componentDidMount() {

    }

    created() {
        // const loadPage = this.props.navigation.getParam('onLoadPage', () => { });
        // loadPage
        this.refs.createModal.showAddModal1()
    }

    render() {
        idPost = this.props.navigation.getParam('idPost', '');
        id = idPost;
        return (
            <View style={{ flex: 1 }}>

                <View style={{ flex: 7, backgroundColor: "#FF6600", flexDirection: 'row' }}>
                    <View>
                        <TouchableOpacity
                            onPress={() => {
                                const loadPage = this.props.navigation.getParam('onLoadPage', () => { });
                                loadPage()
                                this.props.navigation.goBack()
                            }}
                        >
                            <Image style={{ height: 30, width: 30, marginTop: 10 }} source={require("../icon/back.png")}>
                            </Image>
                        </TouchableOpacity>

                    </View>
                    <View>
                        <Text style={{ fontSize: 20, fontWeight: 'bold', marginLeft: 100, marginTop: 7, color: 'white' }}>
                            Đăng Nhà
                        </Text>
                    </View>
                </View>
                <View style={{ flex: 7 }}>
                    <TouchableOpacity
                        onPress={this.created}
                    >
                        <Image style={{ height: 33, width: 33, marginLeft: 160 }} source={require("../icon/add.png")}>
                        </Image>
                    </TouchableOpacity>

                </View>

                <View style={{ flex: 86 }}>
                    <Text style={{ marginLeft: 140, marginTop: 40 }}>Bài đăng mẫu</Text>
                    <Image
                        source={require("../image/huongdan.png")}
                        style={{ width: 250, height: 370, borderRadius: 5, marginLeft: 60, }}
                    >
                    </Image>
                </View>

                <CreateModal ref={'createModal'} parentFlatList={this}>

                </CreateModal>

            </View>
        );
    }
}
