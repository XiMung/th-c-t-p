import React, { Component } from 'react';
import { Image, View, Text, FlatList, Alert } from 'react-native';
import callApi from '../util/apiCaller';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Swipeout from 'react-native-swipeout';
import EditModal from './editHouse';
const uri = "http://192.168.56.1:1998/image/upload/";

export let postId = ''

class FlatListItem extends Component {
    deleteHouse = async () => {
        const id = this.props.item._id
        console.log("========================================>", id);
        await callApi('DELETE', 'http://192.168.56.1:1998/api/deleteHouse', {
            "id": id
        }).then(res => {
            if (res.data.error === false) {
                alert("xoa thành công");
                this.props.parentFlatList.refreshFlatList(id)
            } else {
                alert("loi loi loi !")
            }
        }).catch(err => {
            console.log("fail 222", err);
        })
    }
    render() {
        const { _id } = this.props.item
        const SwipeSeting = {
            autoClose: true,
            onClose: (selectId, rowId, direction) => {
                if (this.state.activeRowKey != null) {
                    this.setState({ activeRowKey: null })
                }
            },
            onOpen: (selectId, rowId, direction) => {
                this.setState({ activeRowKey: this.props.item._id },
                    () => {
                        console.log(this.state.activeRowKey)
                    })
            },
            right: [
                {
                    onPress: () => {
                        postId = _id,
                            this.props.parentFlatList.refs.editModal.showEditModal();
                    },
                    text: 'Sửa', type: 'primary'
                },
                {
                    onPress: () => {
                        // const deletetingRow = this.state.activeRowKey
                        Alert.alert(
                            'Alert',
                            'Bạn có chắc chắn muốn xóa ?',
                            [
                                { text: "No", onPress: () => console.log('Không xóa'), style: 'cancel' },
                                {
                                    text: 'Yes', onPress: async () => {
                                        this.deleteHouse();
                                    }
                                }
                            ],
                            { cancelable: true }
                        );
                    },
                    text: 'Xóa', type: 'delete'
                }
            ],
            rowId: this.props.index,
            sectionId: 1
        };
        return (
            <Swipeout {...SwipeSeting}>
                <View
                    style={{
                        margin: 5,
                        flex: 1,
                        // flexDirection: 'column',
                        // justifyContent: 'flex-start',
                        borderTopLeftRadius: 20,
                        borderTopRightRadius: 20,
                        borderBottomLeftRadius: 20,
                        borderBottomRightRadius: 20,
                        borderWidth: 2,
                        borderColor: "#FF6600",
                    }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginTop: 5 }}>
                        <View style={{ flex: 50 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', }} >
                                <Text style={{ marginTop: 3, marginLeft: 5 }}>Tên:</Text>
                                <Text style={{ fontWeight: "bold", fontSize: 19, marginLeft: 5 }}> {this.props.item.note}</Text>
                            </View>
                        </View>

                        <View style={{ flex: 50 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', }}>
                                <Text style={{ marginTop: 3 }}>Pháp lý:</Text>
                                <Text style={{ fontWeight: "bold", fontSize: 19, marginLeft: 5 }}> {this.props.item.legal}</Text>
                            </View>
                        </View>

                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginTop: 5 }}>
                        <View style={{ flex: 50 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', }} >
                                <Text style={{ marginTop: 3, marginLeft: 5 }}>Diện tích:</Text>
                                <Text style={{ fontWeight: "bold", fontSize: 19, marginLeft: 5 }}> {this.props.item.acreage}</Text>
                            </View>
                        </View>

                        <View style={{ flex: 50 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', }}>
                                <Text style={{ marginTop: 3 }}>Giá:</Text>
                                <Text style={{ fontWeight: "bold", fontSize: 19, marginLeft: 5 }}> {this.props.item.price}</Text>
                            </View>
                        </View>

                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginTop: 5 }}>
                        <View style={{ flex: 50 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', }} >
                                <Text style={{ marginTop: 3, marginLeft: 5 }}>Số lầu:</Text>
                                <Text style={{ fontWeight: "bold", fontSize: 19, marginLeft: 35 }}> {this.props.item.floor}</Text>
                            </View>
                        </View>

                        <View style={{ flex: 50 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', }}>
                                <Text style={{ marginTop: 3 }}>Phòng ngủ:</Text>
                                <Text style={{ fontWeight: "bold", fontSize: 19, marginLeft: 5 }}> {this.props.item.bedroom}</Text>
                            </View>
                        </View>

                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginTop: 5 }}>
                        <View style={{ flex: 50 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', }} >
                                <Text style={{ marginTop: 3, marginLeft: 5 }}>Phòng tắm:</Text>
                                <Text style={{ fontWeight: "bold", fontSize: 19, marginLeft: 5 }}> {this.props.item.bathroom}</Text>
                            </View>
                        </View>

                        <View style={{ flex: 50 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', }}>
                                <Text style={{ marginTop: 3 }}>Ban công:</Text>
                                <Text style={{ fontWeight: "bold", fontSize: 19, marginLeft: 15 }}> {this.props.item.balcony}</Text>
                            </View>
                        </View>

                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginTop: 5 }}>
                        <View style={{ flex: 50 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', }} >
                                <Text style={{ marginTop: 3, marginLeft: 5 }}>Sân thượng:</Text>
                                <Text style={{ fontWeight: "bold", fontSize: 19, marginLeft: 5 }}> {this.props.item.terrace}</Text>
                            </View>
                        </View>

                        <View style={{ flex: 50 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', }}>
                                <Text style={{ marginTop: 3 }}>Sân nhà:</Text>
                                <Text style={{ fontWeight: "bold", fontSize: 19, marginLeft: 25 }}> {this.props.item.placeholder}</Text>
                            </View>
                        </View>

                    </View>

                </View>
            </Swipeout>

        );
    }
}
export default class Detail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            data1: [],
            refreshing: false
        };

    }

    componentDidMount() {
        this.flatList();
        this.superman();
    }

    refreshFlatList = (deletedKey) => {
        const newData = this.state.data.filter((data, index) => data._id != deletedKey)
        this.setState({ data: newData })
    }

    flatList = async () => {
        console.log("000000");
        const idPost = this.props.navigation.getParam('idPost', '')
        console.log("1111 ", idPost);
        await callApi('POST', 'http://192.168.56.1:1998/api/getHousePost', {
            "idPost": idPost
        }).then(res => {
            if (res.data.error === false) {
                this.setState({
                    data: res.data.data
                }, () => console.log("data tra ve: ", this.state.data))
            } else {
                alert("loi loi loi !")
            }
        }).catch(err => {
            console.log("fail 222", err);
        })
    }
    superman = async () => {
        console.log("hieu000000");
        const idPost = this.props.navigation.getParam('idPost', '')
        console.log("hieu1111 ", idPost);
        await callApi('POST', 'http://192.168.56.1:1998/api/getPostId', {
            "id": idPost
        }).then(res => {
            if (res.data.error === false) {
                this.setState({
                    data1: res.data.data
                }, () => console.log("data tra ve hieu: ", this.state.data1))
            } else {
                alert("loi loi loi !")
            }
        }).catch(err => {
            console.log("fail hieu222", err);
        })
    }

    updatedata = async (data) => {
        console.log("----------> ", data);
        const newData = this.state.data.map((item, index) => {
            console.log(">>>>>>>>");
            console.log(item);
            console.log(">>>>>");
            if (item._id === data.data._id) {
                item.note = data.data.note
                item.legal = data.data.legal
                item.acreage = data.data.acreage
                item.price = data.data.price
                item.floor = data.data.floor
                item.balcony = data.data.balcony
                item.bedroom = data.data.bedroom
                item.bathroom = data.data.bathroom
                item.terrace = data.data.terrace
                item.placeholder = data.data.placeholder
            }
            return item
        })

        this.setState({ data: newData })
    }
    render() {
        console.log('hieu log   1  ', this.state.data1);
        return (
            <View style={{ flex: 1 }}>
                {/* <View style={{ flex: 7, backgroundColor: "#FF6600" }}>
                    <View>
                        <Text style={{
                            color: 'white',
                            fontWeight: 'bold',
                            fontSize: 25,
                            marginTop: 6,
                            marginLeft: 130
                        }}>
                            Chi Tiết
                        </Text>
                    </View>
                </View> */}

                <View style={{ flex: 40 }}>
                    <View
                        style={{
                            margin: 10,
                            flex: 1,
                            // flexDirection: 'column',
                            // justifyContent: 'flex-start',
                            borderTopLeftRadius: 20,
                            borderTopRightRadius: 20,
                            borderBottomLeftRadius: 20,
                            borderBottomRightRadius: 20,
                            borderWidth: 1

                        }}>
                        <View style={{ flex: 50, }}>
                            <Image style={{
                                borderTopLeftRadius: 20,
                                borderTopRightRadius: 20,
                                width: 347,
                                height: 118,
                            }}
                                source={{
                                    uri: uri + this.state.data1.image
                                }}
                            />
                        </View>
                        <View style={{ flex: 50 }}>
                            <View style={{
                                flexDirection: 'column',
                                justifyContent: 'flex-start',
                            }}>
                                <View>
                                    <Text style={{ fontWeight: "bold", fontSize: 19, marginLeft: 5 }}> {this.state.data1.title}</Text>
                                </View>
                                <View>
                                    <Text style={{ fontSize: 14, marginLeft: 5 }}> {this.state.data1.address}</Text>
                                </View>
                            </View>

                            <View style={{
                                flexDirection: 'row',
                                justifyContent: 'flex-start'
                            }}>
                                <View style={{ flex: 50, flexDirection: 'row', }}>
                                    <View style={{ fontSize: 5, marginLeft: 5 }}>
                                        <Image source={require("../icon/telephone.png")}>
                                        </Image>
                                    </View>
                                    <View style={{ fontSize: 5, marginLeft: 5 }}>
                                        <Text style={{ fontWeight: "bold", marginTop: 25 }}>{this.state.data1.phone}</Text>
                                    </View>
                                </View>
                                <View style={{ flex: 50, flexDirection: 'row' }}>
                                    <View style={{ fontSize: 5, marginLeft: 5 }}>
                                        <Image source={require("../icon/like.png")}>
                                        </Image>
                                    </View>
                                    <View style={{ fontSize: 5, marginLeft: 5 }}>
                                        <Text style={{ fontWeight: "bold", marginTop: 20 }}>yêu thích</Text>
                                    </View>
                                </View>
                            </View>
                        </View>

                    </View>
                </View>

                <View style={{ flex: 60, backgroundColor: "white" }}>
                    <FlatList
                        data={this.state.data}
                        renderItem={({ item, index }) => {
                            // console.log(`Item = ${JSON.stringify(item)}, Index = ${index}`);
                            return (<FlatListItem item={item} index={index} parentFlatList={this} navigation={this.props.navigation}>

                            </FlatListItem>)
                        }}
                    >

                    </FlatList>


                </View>
                <EditModal ref={'editModal'} updatedata={this.updatedata} parentFlatList={this}>

                </EditModal>
            </View>
        );
    }
}
