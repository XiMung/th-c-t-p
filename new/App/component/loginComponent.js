import React, { Component } from 'react';
import { Image, View, Text, TextInput } from 'react-native';
import { Button } from 'native-base';
import callApi from '../util/apiCaller';
export let role = "";
export let gmail = "";

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: ""
        }
    }

    login = async () => {
        let { email, password } = this.state;
        console.log("success 000", email);
        console.log("success 000", password);
        if (this.state.email.length == 0 || this.state.password.length == 0) {
            alert("vui lòng nhập email hoặc password");
            return;
        }
        await callApi('POST', 'http://192.168.56.1:1998/api/login', {
            "email": email,
            "password": password
        })
            .then(res => {
                console.log("success 1111", res);
                const user = {
                    email: this.state.email
                }
                if (res.data.error === false) {
                    console.log("hahahah", res.data);

                    role = res.data.data[0].idRole.name;
                    gmail = res.data.data[0].email;
                    console.log(">>>>>>>>>>>>>>>>>>>>>>>>");
                    console.log(email);

                    this.props.navigation.navigate('Muabán', user);
                } else {
                    alert("Đăng nhập thất bại");
                }

            }).catch(err => {
                console.log("fail 222", err);
            })
    }
    render() {
        return (
            <View style={{ flex: 1 }}>

                <View style={{ height: 50, backgroundColor: "#FF6600" }}>
                    <View>
                        <Text style={{
                            color: 'white',
                            fontWeight: 'bold',
                            fontSize: 25,
                            marginTop: 10,
                            marginLeft: 125
                        }}>
                            Đăng nhập
                        </Text>
                    </View>
                </View>
                <View style={{ flex: 1, backgroundColor: "white", justifyContent: 'center' }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ justifyContent: 'center' }} >
                            <Image style={{ marginTop: 50, marginLeft: 60 }} source={require("../icon/dangnhap1.png")} />
                        </View>
                        <View style={{ justifyContent: 'center' }} >
                            <Image style={{ marginTop: 10, width: 100, height: 100, }} source={require("../icon/dangnhap.png")} />
                        </View>
                        <View style={{ justifyContent: 'center' }}>
                            <Image style={{ marginTop: 50, }} source={require("../icon/dangnhap1.png")} />
                        </View>
                    </View>
                </View>
                <View style={{ flex: 1, backgroundColor: "white" }}>
                    <View style={{ marginTop: 20 }}>
                        <TextInput style={{
                            height: 50,
                            margin: 10,
                            borderColor: "#FF6600",
                            borderWidth: 2,
                            borderRadius: 10,
                            backgroundColor: "white"
                        }}
                            placeholder='Tên đăng nhập'
                            placeholderTextColor='black'
                            onChangeText={mail => this.setState({ email: mail })}>

                        </TextInput>
                    </View>
                    <View>
                        <TextInput style={{
                            height: 50,
                            margin: 10,
                            borderColor: "#FF6600",
                            borderWidth: 2,
                            borderRadius: 10,
                            backgroundColor: "white"
                        }}
                            placeholder='Mật khẩu'
                            secureTextEntry={true}
                            placeholderTextColor='black'
                            onChangeText={pass => this.setState({ password: pass })}>

                        </TextInput>
                    </View>
                    <View>
                        <Button style={{
                            height: 50,
                            marginTop: 10,
                            marginLeft: 100,
                            width: 150,
                            // borderColor: "black",
                            // borderWidth: 4,
                            borderRadius: 10,
                            backgroundColor: "#FF6600"
                        }}
                            onPress={this.login}

                        // onPress={() => {
                        //     if (this.state.email.length == 0 || this.state.password.length == 0) {
                        //         alert("vui lòng nhập email hoặc password");
                        //         return;
                        //     }
                        //     this.login
                        // }}
                        >
                            <Text style={{ marginLeft: 40, color: 'white' }}>Đăng nhập</Text>
                        </Button>
                    </View>
                    <View style={{ marginTop: 10, alignItems: 'center' }}>
                        <Text style={{ color: "black", fontWeight: "bold" }} onPress={() => { this.props.navigation.navigate('ForgotPass') }}>
                            Quên mật khẩu ?
                        </Text>
                    </View>
                    <View style={{ alignItems: 'center', marginTop: 10 }}>
                        <Text style={{ color: "black", fontWeight: "bold" }} onPress={() => { this.props.navigation.navigate('Register') }}>
                            Đăng ký
                        </Text>
                    </View>

                </View>
            </View>
        );
    }
}
