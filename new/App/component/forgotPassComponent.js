import React, { Component } from 'react';
import { Image, View, Text, TextInput, KeyboardAvoidingView } from 'react-native';
import { Button } from 'native-base';
import callApi from '../util/apiCaller';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default class ForgotPass extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: ""
        }
    }

    ForgotPass = async () => {
        let { email } = this.state;
        if (this.state.email.length == 0) {
            alert("vui lòng nhập email hoặc password");
            return;
        }
        await callApi('POST', 'http://192.168.56.1:1998/api/forgotPassword', {
            "email": email,
        })
            .then(res => {
                if (res.data.error === false) {
                    alert("kiểm tra email của bạn");
                    this.props.navigation.navigate('Login');
                } else {
                    alert("Thất bại");
                }

            }).catch(err => {
                alert("Thất bại");
                console.log("fail 222", err);
            })
    }
    render() {
        return (
            //        <KeyboardAvoidingView behavior='position'>
            <View style={{ flex: 1 }}>

                <View style={{ flex: 10, backgroundColor: "#FF6600" }}>
                    <View>
                        <Text style={{
                            color: 'white',
                            fontWeight: 'bold',
                            fontSize: 25,
                            marginTop: 10,
                            marginLeft: 90
                        }}>
                            Quên mật khẩu
                        </Text>
                    </View>
                </View>
                <View style={{ flex: 30, backgroundColor: "white" }}>
                    <View style={{ flex: 1, flexDirection: 'row', marginTop: 30 }}>
                        <View>
                            <Image style={{ marginTop: 50, marginLeft: 60 }} source={require("../icon/dangnhap1.png")} />
                        </View>
                        <View>
                            <Image style={{ marginTop: 10, width: 100, height: 100, }} source={require("../icon/dangnhap.png")} />
                        </View>
                        <View>
                            <Image style={{ marginTop: 50, }} source={require("../icon/dangnhap1.png")} />
                        </View>
                    </View>
                </View>
                <View style={{ flex: 70, backgroundColor: "white" }}>
                    <View style={{ marginTop: 60 }}>
                        <TextInput style={{
                            height: 50,
                            margin: 10,
                            borderColor: "#FF6600",
                            borderWidth: 2,
                            borderRadius: 10,
                            backgroundColor: "white"
                        }}
                            placeholder='Email của bạn'
                            placeholderTextColor='black'
                            onChangeText={email => this.setState({ email: email })}
                        >

                        </TextInput>
                    </View>

                    <View>
                        <TouchableOpacity
                            onPress={this.ForgotPass}
                        >
                            <Button style={{
                                height: 50,
                                marginTop: 10,
                                marginLeft: 100,
                                width: 150,
                                // borderColor: "black",
                                // borderWidth: 4,
                                borderRadius: 10,
                                backgroundColor: "#FF6600"
                            }}>
                                <Text style={{ marginLeft: 40, color: 'white' }}>Hoàn thành</Text>
                            </Button>
                        </TouchableOpacity>

                    </View>

                </View>
            </View>
            //          </KeyboardAvoidingView>

        );
    }
}
