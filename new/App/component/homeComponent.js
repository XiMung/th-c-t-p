import React, { Component } from 'react';
import { Image, View, Text, TextInput } from 'react-native';
import { Button } from 'native-base';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default class Home extends Component {
    render() {
        return (
            <View style={{ flex: 1 }}>

                <View style={{ flex: 7, backgroundColor: "#FF6600" }}>

                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start', }}>
                        <View>
                            <Text style={{ fontSize: 20, fontWeight: 'bold', marginLeft: 100, marginTop: 7, color: 'white' }}>
                                BẤT ĐỘNG SẢN
                        </Text>
                        </View>
                        <View>
                            <TouchableOpacity
                                onPress={() => { this.props.navigation.navigate('Loc') }}
                            >
                                <Image style={{ width: 30, height: 30, marginLeft: 75, marginTop: 6, tintColor: 'black' }}
                                    source={require("../icon/loc.png")} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={{ flex: 93, backgroundColor: "white" }}>
                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'flex-start',
                        marginTop: 20
                    }}>
                        <View>
                            <TouchableOpacity
                                onPress={() => { this.props.navigation.navigate('NMT') }}
                            >
                                <Image
                                    source={require("../image/2.jpg")}
                                    style={{ width: 150, height: 150, borderRadius: 50, marginLeft: 15 }}
                                >
                                </Image>
                                <Text
                                    style={{ marginLeft: 30, color: "#FF6600", fontSize: 18, fontWeight: 'bold' }}
                                >
                                    Nhà mặt tiền
                            </Text>
                            </TouchableOpacity>

                        </View>
                        <View>
                            <TouchableOpacity onPress={() => { this.props.navigation.navigate('NMH') }}>
                                <Image
                                    source={require("../image/3.jpg")}
                                    style={{ width: 150, height: 150, borderRadius: 50, marginLeft: 30 }}
                                >
                                </Image>
                                <Text
                                    style={{ marginLeft: 50, color: "#FF6600", fontSize: 18, fontWeight: 'bold' }}
                                >
                                    Nhà mặt hẻm
                            </Text>
                            </TouchableOpacity>

                        </View>
                    </View>


                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'flex-start',
                        // backgroundColor: "red",
                        marginTop: 20
                    }}>
                        <View>
                            <TouchableOpacity onPress={() => { this.props.navigation.navigate('BTVL') }}>
                                <Image
                                    source={require("../image/1.jpg")}
                                    style={{ width: 150, height: 150, borderRadius: 50, marginLeft: 15 }}
                                >
                                </Image>
                                <Text
                                    style={{ marginLeft: 30, color: "#FF6600", fontSize: 18, fontWeight: 'bold' }}

                                >
                                    Biệt thự, Villa
                            </Text>
                            </TouchableOpacity>

                        </View>
                        <View>
                            <TouchableOpacity onPress={() => { this.props.navigation.navigate('CCCH') }}>
                                <Image
                                    source={require("../image/4.jpg")}
                                    style={{ width: 150, height: 150, borderRadius: 50, marginLeft: 30 }}
                                >
                                </Image>
                                <Text
                                    style={{ marginLeft: 30, color: "#FF6600", fontSize: 18, fontWeight: 'bold' }}

                                >
                                    Chung cư, căn hộ
                            </Text>
                            </TouchableOpacity>

                        </View>
                    </View>
                    <View style={{ marginTop: 10 }}>
                        <Button style={{
                            height: 50,
                            marginTop: 10,
                            marginLeft: 100,
                            width: 150,
                            borderRadius: 10,
                            backgroundColor: "white",
                            borderColor: "#FF6600",
                            borderWidth: 1,
                        }}
                            onPress={() => { this.props.navigation.navigate('News') }}
                        >
                            <Text style={{
                                color: 'black',
                                fontWeight: 'bold',
                                marginLeft: 20
                            }}>
                                Tất cả tin bán nhà
                            </Text>
                        </Button>
                    </View>
                </View>
            </View >
        );
    }
}