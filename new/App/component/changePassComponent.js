import React, { Component } from 'react';
import { Image, View, Text, TextInput } from 'react-native';
import { Button } from 'native-base';
import { TouchableOpacity } from 'react-native-gesture-handler';
import callApi from '../util/apiCaller';

export default class ChangePass extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            newPass1: "",
            newPass2: ""
        }
    }
    ChangePass = async () => {
        let { email, password, newPass1, newPass2 } = this.state;
        if (this.state.email.length == 0 || this.state.password.length == 0 || this.state.newPass1.length == 0 || this.state.newPass2.length == 0) {
            alert("vui lòng nhập email hoặc password");
            return;
        }
        await callApi('POST', 'http://192.168.56.1:1998/api/changePassword', {
            "email": email,
            "password": password,
            "newPass1": newPass1,
            "newPass2": newPass2
        })
            .then(res => {
                if (res.data.error === false) {
                    alert("Đổi mật khẩu thành công");
                    this.props.navigation.navigate('Login');
                } else {
                    alert("Đổi mật khẩu thất bại");
                }

            }).catch(err => {
                console.log("fail 222", err);
            })
    }
    render() {
        return (
            <View style={{ flex: 1 }}>

                <View style={{ flex: 10, backgroundColor: "#FF6600" }}>
                    <View>
                        <Text style={{
                            color: 'white',
                            fontWeight: 'bold',
                            fontSize: 25,
                            marginTop: 10,
                            marginLeft: 90
                        }}>
                            Đổi mật khẩu
                        </Text>
                    </View>
                </View>
                <View style={{ flex: 20, backgroundColor: "white" }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View>
                            <Image style={{ marginTop: 50, marginLeft: 60 }} source={require("../icon/dangnhap1.png")} />
                        </View>
                        <View>
                            <Image style={{ marginTop: 10, width: 100, height: 100, }} source={require("../icon/dangnhap.png")} />
                        </View>
                        <View>
                            <Image style={{ marginTop: 50, }} source={require("../icon/dangnhap1.png")} />
                        </View>
                    </View>
                </View>
                <View style={{ flex: 80, backgroundColor: "white" }}>
                    <View style={{ marginTop: 20 }}>
                        <TextInput style={{
                            height: 50,
                            margin: 10,
                            borderColor: "#FF6600",
                            borderWidth: 2,
                            borderRadius: 10,
                            backgroundColor: "white"
                        }}
                            placeholder='Email của bạn'
                            placeholderTextColor='black'
                            onChangeText={mail => this.setState({ email: mail })}
                        >

                        </TextInput>
                    </View>
                    <View>
                        <TextInput style={{
                            height: 50,
                            margin: 10,
                            borderColor: "#FF6600",
                            borderWidth: 2,
                            borderRadius: 10,
                            backgroundColor: "white"
                        }}
                            placeholder='Mật khẩu của bạn'
                            placeholderTextColor='black'
                            onChangeText={password => this.setState({ password: password })}
                        >

                        </TextInput>
                    </View>
                    <View>
                        <TextInput style={{
                            height: 50,
                            margin: 10,
                            borderColor: "#FF6600",
                            borderWidth: 2,
                            borderRadius: 10,
                            backgroundColor: "white"
                        }}
                            placeholder='Mật khẩu mới'
                            placeholderTextColor='black'
                            onChangeText={newPass1 => this.setState({ newPass1: newPass1 })}
                        >

                        </TextInput>
                    </View>
                    <View>
                        <TextInput style={{
                            height: 50,
                            margin: 10,
                            borderColor: "#FF6600",
                            borderWidth: 2,
                            borderRadius: 10,
                            backgroundColor: "white"
                        }}
                            placeholder='Nhập lại mật khẩu mới '
                            placeholderTextColor='black'
                            onChangeText={newPass2 => this.setState({ newPass2: newPass2 })}
                        >

                        </TextInput>
                    </View>
                    <View>
                        <TouchableOpacity
                            onPress={this.ChangePass}
                        >
                            <Button style={{
                                height: 50,
                                marginTop: 10,
                                marginLeft: 100,
                                width: 150,
                                borderRadius: 10,
                                backgroundColor: "#FF6600"
                            }}>
                                <Text style={{ marginLeft: 40, color: 'white' }}>Hoàn thành</Text>
                            </Button>
                        </TouchableOpacity>
                    </View>

                </View>
            </View>
        );
    }
}