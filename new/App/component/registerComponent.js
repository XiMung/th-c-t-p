import React, { Component } from 'react';
import { Image, View, Text, TextInput } from 'react-native';
import { Button } from 'native-base';
import callApi from '../util/apiCaller';

export default class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            name: "",
            phone: ""
        }
    }
    register = async () => {
        let { email, password, name, phone } = this.state;
        if (this.state.email.length == 0) {
            alert("vui lòng nhập tên đăng nhập");
            return;
        }
        if (this.state.name.length == 0) {
            alert("vui lòng nhập tên");
            return;
        }
        if (this.state.phone.length == 0) {
            alert("vui lòng nhập số điện thoại");
            return;
        }
        if (this.state.password.length == 0) {
            alert("vui lòng nhập mật khẩu");
            return;
        }
        console.log("hsdhfkashdfkjhsadkfjl");
        console.log(email, password, name, phone);
        await callApi('POST', 'http://192.168.56.1:1998/api/createUser', {
            "email": email,
            "password": password,
            "name": name,
            "phone": phone
        }).then(res => {
            //console.log("success", res.data);
            if (res.data.error === false) {
                alert("Đăng ký thành công");
                this.props.navigation.navigate('Login');
            } else {
                alert("Đăng ký thất bại");
            }

        })
    }
    render() {
        return (
            <View style={{ flex: 1 }}>

                <View style={{ flex: 10, backgroundColor: "#FF6600" }}>
                    <View>
                        <Text style={{
                            color: 'white',
                            fontWeight: 'bold',
                            fontSize: 25,
                            marginTop: 10,
                            marginLeft: 125
                        }}>
                            Đăng ký
                        </Text>
                    </View>
                </View>
                <View style={{ flex: 20, backgroundColor: "white" }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View>
                            <Image style={{ marginTop: 50, marginLeft: 60 }} source={require("../icon/dangnhap1.png")} />
                        </View>
                        <View>
                            <Image style={{ marginTop: 10, width: 100, height: 100, }} source={require("../icon/dangnhap.png")} />
                        </View>
                        <View>
                            <Image style={{ marginTop: 50, }} source={require("../icon/dangnhap1.png")} />
                        </View>
                    </View>
                </View>
                <View style={{ flex: 80, backgroundColor: "white" }}>
                    <View style={{ marginTop: 20 }}>
                        <TextInput style={{
                            height: 50,
                            margin: 10,
                            borderColor: "#FF6600",
                            borderWidth: 2,
                            borderRadius: 10,
                            backgroundColor: "white"
                        }}
                            placeholder='Email đăng ký'
                            placeholderTextColor='black'
                            onChangeText={username => this.setState({ email: username })}
                        >

                        </TextInput>
                    </View>
                    <View>
                        <TextInput style={{
                            height: 50,
                            margin: 10,
                            borderColor: "#FF6600",
                            borderWidth: 2,
                            borderRadius: 10,
                            backgroundColor: "white"
                        }}
                            placeholder='Tên người dùng'
                            placeholderTextColor='black'
                            onChangeText={name => this.setState({ name: name })}
                        >

                        </TextInput>
                    </View>
                    <View>
                        <TextInput style={{
                            height: 50,
                            margin: 10,
                            borderColor: "#FF6600",
                            borderWidth: 2,
                            borderRadius: 10,
                            backgroundColor: "white"
                        }}
                            placeholder='Số điện thoại'
                            placeholderTextColor='black'
                            onChangeText={phone => this.setState({ phone: phone })}
                        >

                        </TextInput>
                    </View>
                    <View>
                        <TextInput style={{
                            height: 50,
                            margin: 10,
                            borderColor: "#FF6600",
                            borderWidth: 2,
                            borderRadius: 10,
                            backgroundColor: "white"
                        }}
                            placeholder='Mật khẩu'
                            placeholderTextColor='black'
                            secureTextEntry={true}
                            onChangeText={password => this.setState({ password: password })}
                        >

                        </TextInput>
                    </View>
                    <View>
                        <Button style={{
                            height: 50,
                            marginTop: 10,
                            marginLeft: 100,
                            width: 150,
                            borderRadius: 10,
                            backgroundColor: "#FF6600"
                        }}
                            onPress={this.register}
                        >
                            <Text style={{ marginLeft: 50, color: 'white' }}>Đăng ký</Text>
                        </Button>
                    </View>

                </View>
            </View>
        );
    }
}
