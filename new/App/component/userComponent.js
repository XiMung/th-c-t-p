import React, { Component } from 'react';
import { Image, View, Text } from 'react-native';
import { ListItem, Thumbnail, Left, Body, Right, Button, Container, Content, List, Icon } from 'native-base';
import { role } from './loginComponent';

export default class User extends Component {
    render() {
        if (role === 'user') {
            return (
                <View style={{ flex: 1 }}>

                    <View style={{ flex: 10, backgroundColor: "#FF6600" }}>
                        <View>
                            <Text style={{
                                color: 'white',
                                fontWeight: 'bold',
                                fontSize: 25,
                                marginTop: 10,
                                marginLeft: 135
                            }}>
                                Cá nhân
                            </Text>
                        </View>
                    </View>
                    <View style={{ flex: 29.5, backgroundColor: "white" }}>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <View>
                                <Image
                                    source={require("../image/2.jpg")}
                                    style={{ width: 100, height: 130, borderRadius: 10, marginTop: 10, marginLeft: 10 }}
                                >
                                </Image>
                            </View>

                            <View>
                                <Text style={{ color: 'black', fontWeight: 'bold', marginTop: 15, marginLeft: 50, fontSize: 20 }}>
                                    Quang Hiếu
                                </Text>
                                <Text style={{ color: '#FF6600', marginTop: 10, marginLeft: 20 }}>
                                    Lã Xuân Oai, Quận 9, Hồ Chí Minh
                                </Text>
                                <Text style={{ color: '#FF6600', marginTop: 5, marginLeft: 20 }}>
                                    quanghieu221998@gmail.com
                                </Text>
                                <Text style={{ color: '#FF6600', marginTop: 10, marginLeft: 20 }}>
                                    0935366620
                                </Text>
                            </View>


                        </View>
                    </View>
                    <View style={{ flex: 0.5, backgroundColor: "black" }}>

                    </View>
                    <View style={{ flex: 70, backgroundColor: "white" }}>
                        <Container>
                            <Content>
                                <List>
                                    <ListItem thumbnail>
                                        <Left>
                                            <Image source={require("../icon/thongtin.png")} />
                                        </Left>
                                        <Body>
                                            <Text style={{ color: '#FF6600', fontWeight: "bold", marginTop: 16 }}>ĐỔI THÔNG TIN CÁ NHÂN</Text>
                                        </Body>
                                        <Right>
                                            <Button transparent onPress={() => { this.props.navigation.navigate('View') }}>
                                                <Image style={{ marginTop: 10 }} source={require("../icon/go.png")}></Image>
                                            </Button>
                                        </Right>
                                    </ListItem>

                                    <ListItem thumbnail>
                                        <Left>
                                            <Image source={require("../icon/pass.png")} />
                                        </Left>
                                        <Body>

                                            <Text style={{ color: '#FF6600', fontWeight: "bold", marginTop: 16 }}>ĐỔI MẬT KHẨU</Text>
                                        </Body>
                                        <Right>
                                            <Button transparent onPress={() => { this.props.navigation.navigate('ChangePass') }}>
                                                <Image style={{ marginTop: 10 }} source={require("../icon/go.png")}></Image>
                                            </Button>
                                        </Right>
                                    </ListItem>

                                    <ListItem thumbnail>
                                        <Left>
                                            <Image source={require("../icon/help.png")} />
                                        </Left>
                                        <Body>

                                            <Text style={{ color: '#FF6600', fontWeight: "bold", marginTop: 16 }}>TRỢ GIÚP</Text>
                                        </Body>
                                        <Right>
                                            <Button transparent onPress={() => { this.props.navigation.navigate('View') }}>
                                                <Image style={{ marginTop: 10 }} source={require("../icon/go.png")}></Image>
                                            </Button>
                                        </Right>
                                    </ListItem>

                                    <ListItem thumbnail>
                                        <Left>
                                            <Image source={require("../icon/caidat.png")} />
                                        </Left>
                                        <Body>

                                            <Text style={{ color: '#FF6600', fontWeight: "bold", marginTop: 16 }}>ĐĂNG XUẤT</Text>
                                        </Body>
                                        <Right>
                                            <Button transparent onPress={() => { this.props.navigation.navigate('View') }}>
                                                <Image style={{ marginTop: 10 }} source={require("../icon/go.png")}></Image>
                                            </Button>
                                        </Right>
                                    </ListItem>
                                </List>
                            </Content>
                        </Container>

                    </View>
                </View>
            );
        } else {
            return (
                <View style={{ flex: 1 }}>

                    <View style={{ flex: 7, backgroundColor: "#FF6600" }}>
                        <View>
                            <Text style={{
                                color: 'white',
                                fontWeight: 'bold',
                                fontSize: 25,
                                marginTop: 7,
                                marginLeft: 135
                            }}>
                                Quản lý
                            </Text>
                        </View>
                    </View>
                    <View style={{ flex: 0.5, backgroundColor: "black" }}>

                    </View>
                    <View style={{ flex: 73, backgroundColor: "white" }}>
                        <Container>
                            <Content>
                                <List>
                                    <ListItem thumbnail>
                                        <Left>
                                            <Image source={require("../icon/thongtin.png")} />
                                        </Left>
                                        <Body>
                                            <Text style={{ color: '#FF6600', fontWeight: "bold", marginTop: 16 }}>Quản Lý Người Dùng</Text>
                                        </Body>
                                        <Right>
                                            <Button transparent onPress={() => { this.props.navigation.navigate('QuanLyUser') }}>
                                                <Image style={{ marginTop: 10 }} source={require("../icon/go.png")}></Image>
                                            </Button>
                                        </Right>
                                    </ListItem>

                                    <ListItem thumbnail>
                                        <Left>
                                            <Image source={require("../icon/pass.png")} />
                                        </Left>
                                        <Body>
                                            <Text style={{ color: '#FF6600', fontWeight: "bold", marginTop: 16 }}>Quản Lý Bài Viết</Text>
                                        </Body>
                                        <Right>
                                            <Button transparent onPress={() => { this.props.navigation.navigate('QuanLyBai') }}>
                                                <Image style={{ marginTop: 10 }} source={require("../icon/go.png")}></Image>
                                            </Button>
                                        </Right>
                                    </ListItem>

                                    <ListItem thumbnail>
                                        <Left>
                                            <Image source={require("../icon/help.png")} />
                                        </Left>
                                        <Body>
                                            <Text style={{ color: '#FF6600', fontWeight: "bold", marginTop: 16 }}>Duyệt Tin</Text>
                                        </Body>
                                        <Right>
                                            <Button transparent onPress={() => { this.props.navigation.navigate('DuyetTin') }}>
                                                <Image style={{ marginTop: 10 }} source={require("../icon/go.png")}></Image>
                                            </Button>
                                        </Right>
                                    </ListItem>

                                    <ListItem thumbnail>
                                        <Left>
                                            <Image source={require("../icon/caidat.png")} />
                                        </Left>
                                        <Body>
                                            <Text style={{ color: '#FF6600', fontWeight: "bold", marginTop: 16 }}>Cấp Quyền</Text>
                                        </Body>
                                        <Right>
                                            <Button transparent onPress={() => { this.props.navigation.navigate('CapQuyen') }}>
                                                <Image style={{ marginTop: 10 }} source={require("../icon/go.png")}></Image>
                                            </Button>
                                        </Right>
                                    </ListItem>
                                </List>
                            </Content>
                        </Container>

                    </View>
                </View>
            );
        }

    }
}
