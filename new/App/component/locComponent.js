import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import callApi from '../util/apiCaller';
import { Button } from 'native-base';
import { TextInput } from 'react-native-gesture-handler';
import { TouchableOpacity } from 'react-native-gesture-handler';
export let acreage = '';
export let price = '';
export let idDistrict = '';
export let idCity = '';

export default class Loc extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            acreage: "",
            price: "",
            idUser: "",
            email: "",
            diachi: "",
            noidung: "",
            tenduong: "",
            idKind: "",
            country: "",
            nameCountry: "",
            idDistrict: "",
            nameDistrict: "",
            idWard: "",
            nameWard: "",
            data: [

            ],
            data1: [

            ],
        };
    }
    componentDidMount() {
        this.List();
    }

    List = async () => {
        await callApi('GET', 'http://192.168.56.1:1998/api/getCity', {

        })
            .then(res => {
                console.log('DATA 00', res.data)
                if (res.data.error === false) {
                    console.log('DATA 11', res.data.data)
                    const dataDropdow = res.data.data.map(data => {
                        console.log('DATA 22', data)
                        return {
                            label: data.name,
                            value: data._id
                        }
                    })

                    console.log('DATA 33', dataDropdow)
                    this.setState({
                        data: dataDropdow
                    }, () => console.log("data tra ve: ", this.state.data))
                }
                // else {
                //     alert("loi loi loi !")
                // }
            }).catch(err => {
                console.log("fail 222", err);
            })
    }

    District = async () => {
        await callApi('POST', 'http://192.168.56.1:1998/api/getDistrictCity', {
            idCity: this.state.country
        })
            .then(res => {
                if (res.data.error === false) {
                    const dataDropdow1 = res.data.data.map(data => {
                        return {
                            label: data.name,
                            value: data._id
                        }
                    })

                    console.log('DATA 33    dataDropdow1', dataDropdow1)
                    this.setState({
                        data1: dataDropdow1
                    }, () => console.log("data tra ve data1: ", this.state.data1))
                }
                //  else {
                //     alert("loi loi loi !")
                // }
            }).catch(err => {
                console.log("fail 222", err);
            })
    }

    onLoadPage = () => {
        this.setState({
            isLoading: true
        })
        this.District()
        this.List()
        setTimeout(() => {
            this.setState({
                isLoading: false
            })
        }, 200)
    }

    render() {
        // nho bo cmt ra khi co dang nhap 
        // const email = this.props.navigation.state.params.email
        if (this.state.isLoading) {
            return <Text>loading..............</Text>
        }
        return (
            <View style={{ flex: 1 }}>

                <View style={{ flex: 6, backgroundColor: "#FF6600", flexDirection: 'row' }}>
                    <View>
                        <TouchableOpacity
                            onPress={() => {
                                this.props.navigation.navigate('Muabán')
                            }}
                        >
                            <Image style={{ height: 30, width: 30, marginTop: 10 }} source={require("../icon/back.png")}>
                            </Image>
                        </TouchableOpacity>

                    </View>
                    <View>
                        <Text style={{ fontSize: 20, fontWeight: 'bold', marginLeft: 130, marginTop: 7, color: 'white' }}>
                            Tìm kiếm
                        </Text>
                    </View>

                </View>
                <View style={{ flex: 5 }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold', marginLeft: 5, color: 'black' }}>
                        Địa chỉ
                        </Text>
                </View>

                <View style={{ flex: 36, backgroundColor: "white" }}>
                    <View style={{
                        flexDirection: 'row',
                        marginTop: 20
                    }}>
                        <View style={{ margin: 10 }}>
                            <Text>Tỉnh/ Thành phố:</Text>
                        </View>
                        <View style={{ marginLeft: 20 }}>
                            <DropDownPicker
                                items={this.state.data}
                                // defaultValue={this.state.data[0].name}
                                containerStyle={{ height: 40, width: 160 }}
                                style={{
                                    borderColor: "#FF6600",
                                    borderWidth: 2,
                                    borderRadius: 10,
                                    backgroundColor: "white"
                                }}
                                itemStyle={{
                                    justifyContent: 'flex-start'
                                }}
                                dropDownStyle={{ backgroundColor: 'yellow' }}
                                onChangeItem={item => this.setState({
                                    country: item.value
                                }, () => this.District())}
                            />
                        </View>

                    </View>

                    <View style={{
                        flexDirection: 'row',
                        marginTop: 20
                    }}>
                        <View style={{ margin: 10 }}>
                            <Text>Quận/ Huyện:</Text>
                        </View>
                        <View style={{ marginLeft: 40 }}>
                            <DropDownPicker
                                items={this.state.data1}
                                // defaultValue={this.state.data[0].name}
                                containerStyle={{ height: 40, width: 160 }}
                                style={{
                                    borderColor: "#FF6600",
                                    borderWidth: 2,
                                    borderRadius: 10,
                                    backgroundColor: "white"
                                }}
                                itemStyle={{
                                    justifyContent: 'flex-start'
                                }}
                                dropDownStyle={{ backgroundColor: 'yellow' }}
                                onChangeItem={item => this.setState({
                                    idDistrict: item.value,
                                })}
                            />
                        </View>

                    </View>
                </View>

                <View style={{ flex: 5 }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold', marginLeft: 5, color: 'black' }}>
                        Giá cả
                        </Text>
                </View>
                <View style={{ flex: 10, backgroundColor: "white" }}>
                    <View style={{ flexDirection: 'row', marginTop: 3 }}>
                        <View style={{ marginTop: 14, marginLeft: 10 }}>
                            <Text>Giá thấp nhất:          </Text>
                        </View>
                        <View style={{ marginLeft: 5 }}>
                            <TextInput style={{
                                height: 40,
                                width: 150,
                                margin: 10,
                                borderColor: "#FF6600",
                                borderWidth: 2,
                                borderRadius: 6,
                                backgroundColor: "white",
                            }}
                                keyboardType="numeric"
                                placeholder='Nhập giá cả'
                                placeholderTextColor='black'
                                onChangeText={price => this.setState({
                                    price: price
                                })}
                            >

                            </TextInput>
                        </View>
                        <View style={{ marginTop: 18 }}>
                            <Text>VNĐ</Text>
                        </View>
                    </View>
                </View>

                <View style={{ flex: 5 }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold', marginLeft: 5, color: 'black' }}>
                        Diện tích
                        </Text>
                </View>

                <View style={{ flex: 23, backgroundColor: "white" }}>

                    <View style={{ flexDirection: 'row', marginTop: 3 }}>
                        <View style={{ marginTop: 14, marginLeft: 10 }}>
                            <Text>Diện tích thấp nhất:</Text>
                        </View>
                        <View style={{ marginLeft: 5 }}>
                            <TextInput style={{
                                height: 40,
                                width: 150,
                                margin: 10,
                                borderColor: "#FF6600",
                                borderWidth: 2,
                                borderRadius: 6,
                                backgroundColor: "white"
                            }}
                                keyboardType="numeric"
                                placeholder='Nhập diện tích'
                                placeholderTextColor='black'
                                onChangeText={acreage => this.setState({
                                    acreage: acreage
                                })}
                            >

                            </TextInput>
                        </View>
                        <View style={{ marginTop: 18 }}>
                            <Text>m2</Text>
                        </View>
                    </View>

                    <View style={{ flex: 15, backgroundColor: "white" }}>
                        <TouchableOpacity
                            onPress={() => {
                                acreage = this.state.acreage;
                                price = this.state.price;
                                idCity = this.state.country;
                                idDistrict = this.state.idDistrict
                                this.props.navigation.navigate('ketquaLoc', { acreage: this.state.acreage, price: this.state.price, idCity: this.state.country, idDistrict: this.state.idDistrict, onLoadPage: this.onLoadPage })
                            }}>
                            <Button style={{
                                height: 50,
                                marginTop: 10,
                                marginLeft: 100,
                                width: 150,
                                borderRadius: 10,
                                backgroundColor: "#FF6600"
                            }}>
                                <Text style={{ marginLeft: 40, color: 'white' }}>Tìm Kiếm</Text>
                            </Button>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}
