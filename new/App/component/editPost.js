import React, { Component } from 'react';
import {
    AppRegistry, FlatList, Text, StyleSheet, View, Image, Alert,
    Platform, Dimensions, TextInput
} from 'react-native';
import Modal from 'react-native-modalbox';
import Button from 'react-native-button';
import { postId } from './myPostComponent';
import callApi from '../util/apiCaller';
import { log } from 'react-native-reanimated';
var screen = Dimensions.get('window');
import DropDownPicker from 'react-native-dropdown-picker';
import { ScrollView } from 'react-native-gesture-handler';
export default class AddModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: "",
            address: "",
            diachi: "",
            noidung: "",
            tenduong: "",
            idKind: "",
            country: "",
            nameCountry: "",
            idDistrict: "",
            nameDistrict: "",
            idWard: "",
            nameWard: "",
            data: [

            ],
            data1: [

            ],
            data2: [

            ],
            data3: [

            ]
        }
    }
    showAddModal = () => {
        this.refs.myModal.open()
    }
    componentDidMount() {
        this.List();
        this.Kind();
    }
    Kind = async () => {
        await callApi('GET', 'http://192.168.56.1:1998/api/getKind', {
        })
            .then(res => {
                if (res.data.error === false) {
                    const dataDropdow3 = res.data.data.map(data => {
                        return {
                            label: data.name,
                            value: data._id
                        }
                    })
                    this.setState({
                        data3: dataDropdow3
                    }, () => console.log("data tra ve data3: ", this.state.data3))
                } else {
                    alert("loi loi loi !")
                }
            }).catch(err => {
                console.log("fail 222", err);
            })
    }

    List = async () => {
        await callApi('GET', 'http://192.168.56.1:1998/api/getCity', {

        })
            .then(res => {
                console.log('DATA 00', res.data)
                if (res.data.error === false) {
                    console.log('DATA 11', res.data.data)
                    const dataDropdow = res.data.data.map(data => {
                        console.log('DATA 22', data)
                        return {
                            label: data.name,
                            value: data._id
                        }
                    })

                    console.log('DATA 33', dataDropdow)
                    this.setState({
                        data: dataDropdow
                    }, () => console.log("data tra ve: ", this.state.data))
                } else {
                    alert("loi loi loi !")
                }
            }).catch(err => {
                console.log("fail 222", err);
            })
    }

    District = async () => {
        await callApi('POST', 'http://192.168.56.1:1998/api/getDistrictCity', {
            idCity: this.state.country
        })
            .then(res => {
                if (res.data.error === false) {
                    const dataDropdow1 = res.data.data.map(data => {
                        return {
                            label: data.name,
                            value: data._id
                        }
                    })

                    console.log('DATA 33    dataDropdow1', dataDropdow1)
                    this.setState({
                        data1: dataDropdow1
                    }, () => console.log("data tra ve data1: ", this.state.data1))
                } else {
                    alert("loi loi loi !")
                }
            }).catch(err => {
                console.log("fail 222", err);
            })
    }

    Ward = async () => {
        await callApi('POST', 'http://192.168.56.1:1998/api/getWardDistrict', {
            idDistrict: this.state.idDistrict
        })
            .then(res => {
                if (res.data.error === false) {
                    const dataDropdow2 = res.data.data.map(data => {
                        return {
                            label: data.name,
                            value: data._id
                        }
                    })

                    console.log('DATA 33    dataDropdow1', dataDropdow2)
                    this.setState({
                        data2: dataDropdow2
                    }, () => console.log("data tra ve data2: ", this.state.data2))
                } else {
                    alert("loi loi loi !")
                }
            }).catch(err => {
                console.log("fail 222", err);
            })
    }
    render() {
        console.log('1111idPostidPostidPost', postId)
        return (
            <Modal
                ref={"myModal"}
                style={{
                    justifyContent: 'center',
                    borderRadius: 13,
                    shadowRadius: 10,
                    width: screen.width - 80,
                    height: 440
                }}
                position='center'
                backdrop={true}
                onClosed={() => {

                }}
            >

                <View style={{ flex: 1, backgroundColor: "white", borderRadius: 10, }}>
                    <View style={{ height: 10 }}>
                        <Text style={{
                            fontSize: 16,
                            fontWeight: 'bold',
                            textAlign: 'center',
                        }}
                        >Sửa bài đăng</Text>
                    </View>
                    <ScrollView style={{ flex: 1 }}>
                        <View style={{ flexDirection: 'row', marginTop: 3 }}>
                            <View style={{ marginTop: 14, marginLeft: 10 }}>
                                <Text>Nội dung:</Text>
                            </View>
                            <View style={{ marginLeft: 5 }}>
                                <TextInput style={{
                                    height: 40,
                                    width: 200,
                                    margin: 10,
                                    borderColor: "#FF6600",
                                    borderWidth: 2,
                                    borderRadius: 6,
                                    backgroundColor: "white"
                                }}
                                    placeholder='Nhập nội dung'
                                    placeholderTextColor='black'
                                    onChangeText={noidung => this.setState({
                                        noidung: noidung,
                                        diachi: this.state.tenduong + ", " + this.state.nameWard + ", " + this.state.nameDistrict + ", " + this.state.nameCountry,
                                    })}
                                >

                                </TextInput>
                            </View>
                        </View>

                        <View style={{
                            flexDirection: 'row',
                            marginTop: 10
                        }}>
                            <View style={{ margin: 10 }}>
                                <Text>Loại nhà:</Text>
                            </View>
                            <View style={{ marginLeft: 70 }}>
                                <DropDownPicker
                                    items={this.state.data3}
                                    // defaultValue={}
                                    containerStyle={{ height: 40, width: 130 }}
                                    style={{
                                        borderColor: "#FF6600",
                                        borderWidth: 2,
                                        borderRadius: 10,
                                        backgroundColor: "white"
                                    }}
                                    itemStyle={{
                                        justifyContent: 'flex-start'
                                    }}
                                    dropDownStyle={{ backgroundColor: '#DDDDDD' }}
                                    onChangeItem={item => this.setState({
                                        idKind: item.value
                                    })}
                                />
                            </View>

                        </View>

                        <View style={{ flexDirection: 'row', marginTop: 10 }}>
                            <View style={{ marginTop: 20, marginLeft: 10 }}>
                                <Text>Đường/ Hẻm:</Text>
                            </View>
                            <View style={{ marginLeft: 40 }}>
                                <TextInput style={{
                                    height: 40,
                                    width: 130,
                                    margin: 10,
                                    borderColor: "#FF6600",
                                    borderWidth: 2,
                                    borderRadius: 6,
                                    backgroundColor: "white"
                                }}
                                    placeholder='Nhập tên đường'
                                    placeholderTextColor='black'
                                    onChangeText={tenduong => this.setState({
                                        tenduong: tenduong
                                    })}
                                >

                                </TextInput>
                            </View>
                        </View>

                        <View style={{
                            flexDirection: 'row',
                            marginTop: 17
                        }}>
                            <View style={{ margin: 10 }}>
                                <Text>Tỉnh/ Thành phố:</Text>
                            </View>
                            <View style={{ marginLeft: 20.5 }}>
                                <DropDownPicker
                                    items={this.state.data}
                                    // defaultValue={this.state.data[0].name}
                                    containerStyle={{ height: 40, width: 130 }}
                                    style={{
                                        borderColor: "#FF6600",
                                        borderWidth: 2,
                                        borderRadius: 10,
                                        backgroundColor: "white"
                                    }}
                                    itemStyle={{
                                        justifyContent: 'flex-start'
                                    }}
                                    dropDownStyle={{ backgroundColor: '#DDDDDD' }}
                                    onChangeItem={item => this.setState({
                                        country: item.value,
                                        nameCountry: item.label
                                    }, () => this.District())}
                                />
                            </View>

                        </View>

                        <View style={{
                            flexDirection: 'row',
                            marginTop: 20
                        }}>
                            <View style={{ margin: 10 }}>
                                <Text>Quận/ Huyện:</Text>
                            </View>
                            <View style={{ marginLeft: 40 }}>
                                <DropDownPicker
                                    items={this.state.data1}
                                    // defaultValue={this.state.data[0].name}
                                    containerStyle={{ height: 40, width: 130 }}
                                    style={{
                                        borderColor: "#FF6600",
                                        borderWidth: 2,
                                        borderRadius: 10,
                                        backgroundColor: "white"
                                    }}
                                    itemStyle={{
                                        justifyContent: 'flex-start'
                                    }}
                                    dropDownStyle={{ backgroundColor: '#DDDDDD' }}
                                    onChangeItem={item => this.setState({
                                        idDistrict: item.value,
                                        nameDistrict: item.label
                                    }, () => this.Ward())}
                                />
                            </View>

                        </View>

                        <View style={{ flexDirection: 'row', marginTop: 20 }}>
                            <View style={{ margin: 10 }}>
                                <Text>Phường/ Xã:</Text>
                            </View>
                            <View style={{ marginLeft: 47 }}>
                                <DropDownPicker
                                    items={this.state.data2}
                                    // defaultValue={this.state.data[0].name}
                                    containerStyle={{ height: 40, width: 130 }}
                                    style={{
                                        borderColor: "#FF6600",
                                        borderWidth: 2,
                                        borderRadius: 10,
                                        backgroundColor: "white"
                                    }}
                                    itemStyle={{
                                        justifyContent: 'flex-start'
                                    }}
                                    dropDownStyle={{ backgroundColor: '#DDDDDD' }}
                                    onChangeItem={item => this.setState({
                                        idWard: item.value,
                                        nameWard: item.label
                                    })}
                                />
                            </View>

                        </View>

                        <View style={{ marginTop: 10 }}>
                            <Button
                                style={{ fontSize: 18, color: 'white' }}
                                containerStyle={{
                                    padding: 8,
                                    marginLeft: 70,
                                    marginRight: 70,
                                    height: 40,
                                    borderRadius: 6,
                                    backgroundColor: 'mediumseagreen'
                                }}
                                onPress={async () => {
                                    let { nameCountry, nameDistrict, nameWard, tenduong } = this.state
                                    if (nameCountry === "") {
                                        nameDistrict = nameWard = tenduong = ""
                                    } else {
                                        if (nameDistrict === "" || nameWard === "" || tenduong === "") {
                                            alert("Bạn hãy nhập đầy đủ địa chỉ");
                                            return;
                                        }
                                    }
                                    // let { diachi } = this.state
                                    let diachi = tenduong + ", " + nameWard + ", " + nameDistrict + ", " + nameCountry
                                    console.log("hieu2222" + diachi + "222");
                                    if (diachi === ", , , ") {
                                        diachi = "";
                                    }
                                    // const { noidung, diachi } = this.state
                                    let { noidung } = this.state
                                    let { idKind } = this.state

                                    await callApi('PATCH', 'http://192.168.56.1:1998/api/updatePost', {
                                        "idKind": idKind,
                                        "id": postId,
                                        "title": noidung,
                                        "address": diachi
                                    }).then(res => {
                                        if (res.data.err === false) {
                                            this.props.updatedata({ data: res.data.data })
                                            alert("Cập nhật thành công");
                                            this.refs.myModal.close();
                                        }
                                    }).catch(err => {
                                        console.log("fail 222", err);
                                    })
                                    // this.state.FlatListItem.refreshFlatListItem()

                                }}
                            >Lưu</Button>
                        </View>
                    </ScrollView>

                </View>

            </Modal>
        );
    }
}