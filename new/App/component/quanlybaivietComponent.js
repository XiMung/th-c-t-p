import React, { Component } from 'react';
import { Image, View, Text, FlatList, Alert } from 'react-native';
import callApi from '../util/apiCaller';
import { TouchableOpacity } from 'react-native-gesture-handler';
// import Swipeout from 'react-native-swipeout';
import AddModal from './editPostQuanLyBai';
const uri = "http://192.168.56.1:1998/image/upload/";

export let postId = ''

class FlatListItem extends Component {
    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         deletedRowKey: null,
    //         activeRowKey: null,
    //     }
    // }
    // deletePost = async () => {
    //     const id = this.props.item._id
    //     console.log("========================================>", id);
    //     await callApi('DELETE', 'http://192.168.56.1:1998/api/deletePost', {
    //         "id": id
    //     }).then(res => {
    //         if (res.data.error === false) {
    //             alert("xoa thành công");
    //             this.props.parentFlatList.refreshFlatList(id)
    //         } else {
    //             alert("loi loi loi !")
    //         }
    //     }).catch(err => {
    //         console.log("fail 222", err);
    //     })
    // }

    Duyet = async () => {
        let verify = this.props.parentFlatList.state.verify
        console.log("const verify = >> ", verify);
        const { _id } = this.props.item;
        await callApi('PATCH', 'http://192.168.56.1:1998/api/updatePost', {
            "id": _id,
            "verify": 1,
        }).then(res => {
            if (res.data.err === false) {
                alert("thành công");
                let data1 = res.data.data;
                this.props.parentFlatList.refreshFlatList(data1)
            } else {
                alert(" Thất bại");
            }
        }).catch(err => {
            console.log("fail 222", err);
            alert(" Thất bại");
        })
    }
    render() {
        const { _id } = this.props.item
        return (
            // <Swipeout {...SwipeSeting}>
            <View
                style={{
                    margin: 10,
                    flex: 1,
                    borderTopLeftRadius: 20,
                    borderTopRightRadius: 20,
                    borderBottomLeftRadius: 20,
                    borderBottomRightRadius: 20,
                    borderWidth: 1

                }}>
                <View style={{ flex: 50, }}>
                    <Image style={{
                        borderTopLeftRadius: 20,
                        borderTopRightRadius: 20,
                        width: 347,
                        height: 200,
                    }}
                        source={{
                            uri: uri + this.props.item.image
                        }}
                    />
                </View>
                <View style={{ flex: 50 }}>
                    <View style={{
                        flexDirection: 'column',
                        justifyContent: 'flex-start',
                        marginTop: 10
                    }}>
                        <View>
                            <Text style={{ fontWeight: "bold", fontSize: 19, marginLeft: 5 }}> {this.props.item.title}</Text>
                        </View>
                        <View>
                            <Text style={{ fontSize: 14, marginLeft: 5 }}> {this.props.item.address}</Text>
                        </View>
                    </View>
                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'flex-start'
                    }}>
                        <View style={{ flex: 50, flexDirection: 'row', backgroundColor: 'yellow' }}>
                            <View style={{ fontSize: 5, marginLeft: 5 }}>
                                <Image source={require("../icon/money.png")}>
                                </Image>
                            </View>
                            <View style={{ fontSize: 5, }}>
                                <Text style={{ fontWeight: "bold", marginTop: 20 }}>5,2 tỷ</Text>
                            </View>
                        </View>
                        <View style={{ flex: 50, flexDirection: 'row' }}>
                            <View style={{ fontSize: 5, marginLeft: 5 }}>
                                <Image source={require("../icon/people.png")}>
                                </Image>
                            </View>
                            <View style={{ fontSize: 5, marginLeft: 5 }}>
                                <Text style={{ fontWeight: "bold", marginTop: 25 }}>{this.props.item.idUser.name}</Text>
                            </View>
                        </View>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'flex-start'
                    }}>
                        <View style={{ flex: 50, flexDirection: 'row', }}>
                            <View style={{ fontSize: 5, marginLeft: 5 }}>
                                <Image source={require("../icon/telephone.png")}>
                                </Image>
                            </View>
                            <View style={{ fontSize: 5, marginLeft: 5 }}>
                                <Text style={{ fontWeight: "bold", marginTop: 25 }}>{this.props.item.idUser.phone}</Text>
                            </View>
                        </View>
                        <View style={{ flex: 50, flexDirection: 'row' }}>
                            <View style={{ fontSize: 5, marginLeft: 5 }}>
                                <TouchableOpacity onPress={() => {
                                    this.Duyet();
                                }}>
                                    <Image source={require("../icon/duyet.png")}>
                                    </Image>
                                </TouchableOpacity>
                            </View>
                            <View style={{ fontSize: 5, marginLeft: 5 }}>
                                <Text style={{ fontWeight: "bold", marginTop: 20 }}>Hủy duyệt</Text>
                            </View>
                        </View>
                    </View>
                    <View>
                        <TouchableOpacity
                            onPress={() => {
                                console.log('duc 4444', this.props.navigation)
                                this.props.navigation.navigate('Detail1', { idPost: _id })
                            }}
                        >
                            <Text style={{ color: "#FF6600", marginLeft: 135, fontWeight: "bold" }}>Xem chi tiết</Text>
                        </TouchableOpacity>

                    </View>
                </View>

            </View>
            // </Swipeout>
        );
    }
}
export default class QuanLyBai extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFetching: false,
            data: [],
            refreshing: false,
            deletedRowKey: null,
        };
        this.flatList();
    }

    refreshFlatList = (deletedKey) => {
        const newData = this.state.data.filter((data, index) => data._id != deletedKey)
        this.setState({ data: newData })
    }

    flatList = async () => {
        this.setState({ isFetching: true })
        await callApi('GET', 'http://192.168.56.1:1998/api/getPost', {
            // "email": email
        }).then(res => {
            if (res.data.error === false) {
                this.setState({
                    data: res.data.data,
                    isFetching: false
                }, () => console.log("data tra ve: ", this.state.data))
            } else {
                alert("loi loi loi !")
            }
        }).catch(err => {
            console.log("fail 222", err);
        })
    }

    updatedata = async (data) => {
        console.log("----------> ", data);
        const newData = this.state.data.map((item, index) => {
            if (item._id == data.data._id) {
                item.title = data.data.title
                item.address = data.data.address
            }
            return item
        })

        this.setState({ data: newData })
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 7, backgroundColor: "#FF6600", flexDirection: 'row' }}>
                    <View>
                        <TouchableOpacity
                            onPress={() => {
                                this.props.navigation.navigate('Cánhân')
                            }}
                        >
                            <Image style={{ height: 30, width: 30, marginTop: 10 }} source={require("../icon/back.png")}>
                            </Image>
                        </TouchableOpacity>

                    </View>
                    <View>
                        <Text style={{
                            color: 'white',
                            fontWeight: 'bold',
                            fontSize: 25,
                            marginTop: 6,
                            marginLeft: 110
                        }}>
                            Quản Lý Tin
                        </Text>
                    </View>
                </View>
                <View style={{ flex: 93, backgroundColor: "white" }}>
                    <FlatList
                        onRefresh={() => this.flatList()}
                        refreshing={this.state.isFetching}
                        data={this.state.data}
                        renderItem={({ item, index }) => {
                            console.log(`Item = ${JSON.stringify(item)}, Index = ${index}`);
                            return (<FlatListItem item={item} index={index} parentFlatList={this} navigation={this.props.navigation} >

                            </FlatListItem>)
                        }}
                    >
                    </FlatList>
                    <AddModal ref={'addModal'} updatedata={this.updatedata} parentFlatList={this}>

                    </AddModal>
                </View>
            </View>
        );
    }
}
