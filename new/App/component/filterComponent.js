import React, { Component } from 'react';
import { View, Text } from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import callApi from '../util/apiCaller';
import { Button } from 'native-base';
import { TextInput, ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { gmail } from './loginComponent';

export default class filterComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            idUser: "",
            email: "",
            diachi: "",
            noidung: "",
            tenduong: "",
            idKind: "",
            country: "",
            nameCountry: "",
            idDistrict: "",
            nameDistrict: "",
            idWard: "",
            nameWard: "",
            data: [

            ],
            data1: [

            ],
            data2: [

            ],
            data3: [

            ]
        };
    }

    componentDidMount() {
        this.List();
        this.Kind();
    }

    Kind = async () => {
        await callApi('GET', 'http://192.168.56.1:1998/api/getKind', {
        })
            .then(res => {
                if (res.data.error === false) {
                    const dataDropdow3 = res.data.data.map(data => {
                        return {
                            label: data.name,
                            value: data._id
                        }
                    })
                    this.setState({
                        data3: dataDropdow3
                    }, () => console.log("data tra ve data3: ", this.state.data3))
                }
                // else {
                //     alert("loi loi loi !")
                // }
            }).catch(err => {
                console.log("fail 222", err);
            })
    }

    List = async () => {
        await callApi('GET', 'http://192.168.56.1:1998/api/getCity', {

        })
            .then(res => {
                console.log('DATA 00', res.data)
                if (res.data.error === false) {
                    console.log('DATA 11', res.data.data)
                    const dataDropdow = res.data.data.map(data => {
                        console.log('DATA 22', data)
                        return {
                            label: data.name,
                            value: data._id
                        }
                    })

                    console.log('DATA 33', dataDropdow)
                    this.setState({
                        data: dataDropdow
                    }, () => console.log("data tra ve: ", this.state.data))
                }
                // else {
                //     alert("loi loi loi !")
                // }
            }).catch(err => {
                console.log("fail 222", err);
            })
    }

    District = async () => {
        await callApi('POST', 'http://192.168.56.1:1998/api/getDistrictCity', {
            idCity: this.state.country
        })
            .then(res => {
                if (res.data.error === false) {
                    const dataDropdow1 = res.data.data.map(data => {
                        return {
                            label: data.name,
                            value: data._id
                        }
                    })

                    console.log('DATA 33    dataDropdow1', dataDropdow1)
                    this.setState({
                        data1: dataDropdow1
                    }, () => console.log("data tra ve data1: ", this.state.data1))
                }
                // else {
                //     alert("loi loi loi !")
                // }
            }).catch(err => {
                console.log("fail 222", err);
            })
    }

    Ward = async () => {
        await callApi('POST', 'http://192.168.56.1:1998/api/getWardDistrict', {
            idDistrict: this.state.idDistrict
        })
            .then(res => {
                if (res.data.error === false) {
                    const dataDropdow2 = res.data.data.map(data => {
                        return {
                            label: data.name,
                            value: data._id
                        }
                    })

                    console.log('DATA 33    dataDropdow1', dataDropdow2)
                    this.setState({
                        data2: dataDropdow2
                    }, () => console.log("data tra ve data2: ", this.state.data2))
                }
                // else {
                //     alert("loi loi loi !")
                // }
            }).catch(err => {
                console.log("fail 222", err);
            })
    }

    IdUser = async () => {
        await callApi('POST', 'http://192.168.56.1:1998/api/getId', {
            email: this.state.email
        })
            .then(res => {
                if (res.data.error === false) {
                    this.setState({
                        idUser: res.data.data
                    }, () => console.log("data tra ve idUser-----: ", this.state.idUser))

                }
                //  else {
                //     alert("loi loi loi !")
                // }
            }).catch(err => {
                console.log("fail 222", err);
            })
    }

    Created = async () => {
        await callApi('POST', 'http://192.168.56.1:1998/api/createPost', {
            title: this.state.noidung,
            address: this.state.diachi,
            idUser: this.state.idUser,
            idKind: this.state.idKind,
            idDistrict: this.state.idDistrict,
        })
            .then(res => {
                if (res.data.error === false) {
                    console.log("hhhhhhhhhhhh      >" + res.data.data._id);
                    let idPost = res.data.data._id;
                    // alert("Thành công !")
                    this.props.navigation.navigate('CreateHouse', { idPost: idPost, onLoadPage: this.onLoadPage });
                    this.setState({
                        idKind: "",
                        country: "",
                        idDistrict: "",
                        idWard: "",
                        diachi: "",
                        noidung: "",
                    })
                } else {
                    // alert("Thất bại !")
                }
            }).catch(err => {
                console.log("Thất bại", err);
            })
    }

    onLoadPage = () => {
        this.setState({
            isLoading: true
        })
        this.District()
        this.Ward()
        this.Kind()
        this.List()
        setTimeout(() => {
            this.setState({
                isLoading: false
            })
        }, 200)
    }

    render() {
        // nho bo cmt ra khi co dang nhap 
        // let email = this.props.navigation.state.params.email
        console.log("////////////////////////", gmail);
        if (this.state.isLoading) {
            return <Text>loading..............</Text>
        }

        return (
            <View style={{ flex: 1 }}>

                <View style={{ backgroundColor: "#FF6600", height: 40 }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold', marginLeft: 150, marginTop: 7, color: 'white' }}>
                        Đăng Bài
                        </Text>
                </View>



                <ScrollView style={{ flex: 1, backgroundColor: "white" }}>
                    <View style={{
                        flexDirection: 'row',
                        marginTop: 20
                    }}>
                        <View style={{ margin: 10 }}>
                            <Text>Loại nhà:</Text>
                        </View>
                        <View style={{ marginLeft: 70 }}>
                            <DropDownPicker
                                items={this.state.data3}
                                // defaultValue={}
                                containerStyle={{ height: 40, width: 160 }}
                                style={{
                                    borderColor: "#FF6600",
                                    borderWidth: 2,
                                    borderRadius: 10,
                                    backgroundColor: "white"
                                }}
                                itemStyle={{
                                    justifyContent: 'flex-start'
                                }}

                                dropDownStyle={{ backgroundColor: '#DDDDDD' }}
                                onChangeItem={item => this.setState({
                                    idKind: item.value
                                })}
                            />
                        </View>

                    </View>

                    <View style={{
                        flexDirection: 'row',
                        marginTop: 20
                    }}>
                        <View style={{ margin: 10 }}>
                            <Text>Tỉnh/ Thành phố:</Text>
                        </View>
                        <View style={{ marginLeft: 20 }}>
                            <DropDownPicker
                                items={this.state.data}
                                // defaultValue={this.state.data[0].name}
                                containerStyle={{ height: 40, width: 160 }}
                                style={{
                                    borderColor: "#FF6600",
                                    borderWidth: 2,
                                    borderRadius: 10,
                                    backgroundColor: "white"
                                }}
                                itemStyle={{
                                    justifyContent: 'flex-start'
                                }}
                                dropDownStyle={{ backgroundColor: '#DDDDDD' }}
                                onChangeItem={item => this.setState({
                                    country: item.value,
                                    nameCountry: item.label
                                }, () => this.District())}
                            />
                        </View>

                    </View>

                    <View style={{
                        flexDirection: 'row',
                        marginTop: 20
                    }}>
                        <View style={{ margin: 10 }}>
                            <Text>Quận/ Huyện:</Text>
                        </View>
                        <View style={{ marginLeft: 40 }}>
                            <DropDownPicker
                                items={this.state.data1}
                                // defaultValue={this.state.data[0].name}
                                containerStyle={{ height: 40, width: 160 }}
                                style={{
                                    borderColor: "#FF6600",
                                    borderWidth: 2,
                                    borderRadius: 10,
                                    backgroundColor: "white"
                                }}
                                itemStyle={{
                                    justifyContent: 'flex-start'
                                }}
                                dropDownStyle={{ backgroundColor: '#DDDDDD' }}
                                onChangeItem={item => this.setState({
                                    idDistrict: item.value,
                                    nameDistrict: item.label
                                }, () => this.Ward())}
                            />
                        </View>

                    </View>

                    <View style={{ flexDirection: 'row', marginTop: 20 }}>
                        <View style={{ margin: 10 }}>
                            <Text>Phường/ Xã:</Text>
                        </View>
                        <View style={{ marginLeft: 47 }}>
                            <DropDownPicker
                                items={this.state.data2}
                                // defaultValue={this.state.data[0].name}
                                containerStyle={{ height: 40, width: 160 }}
                                style={{
                                    borderColor: "#FF6600",
                                    borderWidth: 2,
                                    borderRadius: 10,
                                    backgroundColor: "white"
                                }}
                                itemStyle={{
                                    justifyContent: 'flex-start'
                                }}
                                dropDownStyle={{ backgroundColor: '#DDDDDD' }}
                                onChangeItem={item => this.setState({
                                    idWard: item.value,
                                    nameWard: item.label
                                })}
                            />
                        </View>

                    </View>

                    <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <View style={{ marginTop: 20, marginLeft: 10 }}>
                            <Text>Đường/ Hẻm:</Text>
                        </View>
                        <View style={{ marginLeft: 40 }}>
                            <TextInput style={{
                                height: 40,
                                width: 160,
                                margin: 10,
                                borderColor: "#FF6600",
                                borderWidth: 2,
                                borderRadius: 6,
                                backgroundColor: "white"
                            }}
                                placeholder='Nhập tên đường'
                                placeholderTextColor='black'
                                onChangeText={tenduong => this.setState({
                                    tenduong: tenduong
                                })}
                            >

                            </TextInput>
                        </View>
                    </View>

                    <View style={{ flexDirection: 'row', marginTop: 3 }}>
                        <View style={{ marginTop: 14, marginLeft: 10 }}>
                            <Text>Nội dung:</Text>
                        </View>
                        <View style={{ marginLeft: 5 }}>
                            <TextInput style={{
                                height: 40,
                                width: 250,
                                margin: 10,
                                borderColor: "#FF6600",
                                borderWidth: 2,
                                borderRadius: 6,
                                backgroundColor: "white"
                            }}
                                placeholder='Nhập nội dung'
                                placeholderTextColor='black'
                                onChangeText={noidung => this.setState({
                                    noidung: noidung,
                                    diachi: this.state.tenduong + ", " + this.state.nameWard + ", " + this.state.nameDistrict + ", " + this.state.nameCountry,
                                    email: gmail
                                }, () => this.IdUser())}
                            >

                            </TextInput>
                        </View>
                    </View>



                    <View>
                        <Button style={{
                            height: 50,
                            marginTop: 10,
                            marginLeft: 100,
                            width: 150,
                            borderRadius: 10,
                            backgroundColor: "#FF6600"
                        }}
                            onPress={this.Created}
                        >
                            <Text style={{ marginLeft: 40, color: 'white' }}>Thêm nhà</Text>
                        </Button>
                    </View>
                </ScrollView>

            </View>
        );
    }
}
