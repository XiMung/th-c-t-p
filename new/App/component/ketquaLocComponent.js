import React, { Component } from 'react';
import { Image, View, Text, FlatList } from 'react-native';
import callApi from '../util/apiCaller';
import { TouchableOpacity } from 'react-native-gesture-handler';
const uri = "http://192.168.56.1:1998/image/upload/";
import { acreage } from "./locComponent";
import { price } from "./locComponent";
import { idDistrict } from "./locComponent";
import { idCity } from "./locComponent";

class FlatListItem extends Component {
    render() {
        const { _id } = this.props.item

        console.log('duc 22', _id)
        return (
            <View
                style={{
                    margin: 10,
                    flex: 1,
                    // flexDirection: 'column',
                    // justifyContent: 'flex-start',
                    borderTopLeftRadius: 20,
                    borderTopRightRadius: 20,
                    borderBottomLeftRadius: 20,
                    borderBottomRightRadius: 20,
                    borderWidth: 1

                }}>
                <View style={{ flex: 50, }}>
                    <Image style={{
                        borderTopLeftRadius: 20,
                        borderTopRightRadius: 20,
                        width: 347,
                        height: 200,
                    }}
                        source={{
                            uri: uri + this.props.item.image
                        }}
                    />
                </View>
                <View style={{ flex: 50 }}>
                    <View style={{
                        flexDirection: 'column',
                        justifyContent: 'flex-start',
                        marginTop: 10
                    }}>
                        <View>
                            <Text style={{ fontWeight: "bold", fontSize: 19, marginLeft: 5 }}> {this.props.item.title}</Text>
                        </View>
                        <View>
                            <Text style={{ fontSize: 14, marginLeft: 5 }}> {this.props.item.address}</Text>
                        </View>
                    </View>
                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'flex-start'
                    }}>
                        <View style={{ flex: 50, flexDirection: 'row', backgroundColor: 'yellow' }}>
                            <View style={{ fontSize: 5, marginLeft: 5 }}>
                                <Image source={require("../icon/money.png")}>
                                </Image>
                            </View>
                            <View style={{ fontSize: 5, }}>
                                <Text style={{ fontWeight: "bold", marginTop: 20 }}>{this.props.item.minPrice}</Text>
                            </View>
                        </View>
                        <View style={{ flex: 50, flexDirection: 'row' }}>
                            <View style={{ fontSize: 5, marginLeft: 5 }}>
                                <Image source={require("../icon/people.png")}>
                                </Image>
                            </View>
                            <View style={{ fontSize: 5, marginLeft: 5 }}>
                                <Text style={{ fontWeight: "bold", marginTop: 25 }}>{this.props.item.idUser.name}</Text>
                            </View>
                        </View>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'flex-start'
                    }}>
                        <View style={{ flex: 50, flexDirection: 'row', }}>
                            <View style={{ fontSize: 5, marginLeft: 5 }}>
                                <Image source={require("../icon/telephone.png")}>
                                </Image>
                            </View>
                            <View style={{ fontSize: 5, marginLeft: 5 }}>
                                <Text style={{ fontWeight: "bold", marginTop: 25 }}>{this.props.item.idUser.phone}</Text>
                            </View>
                        </View>
                        <View style={{ flex: 50, flexDirection: 'row' }}>
                            <View style={{ fontSize: 5, marginLeft: 5 }}>
                                <Image source={require("../icon/like.png")}>
                                </Image>
                            </View>
                            <View style={{ fontSize: 5, marginLeft: 5 }}>
                                <Text style={{ fontWeight: "bold", marginTop: 20 }}>yêu thích</Text>
                            </View>
                        </View>
                    </View>
                    <View>
                        <TouchableOpacity
                            onPress={() => {
                                console.log('duc 4444', this.props.navigation)
                                this.props.navigation.navigate('Detail1', { idPost: _id })
                            }}
                        >
                            <Text style={{ color: "#FF6600", marginLeft: 135, fontWeight: "bold" }}>Xem chi tiết</Text>
                        </TouchableOpacity>

                    </View>
                </View>

            </View>
        );
    }
}
export default class ketquaLoc extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            refreshing: false
        };
        this.flatList();
    }
    flatList = async () => {
        let { } = this.state;
        await callApi('POST', 'http://192.168.56.1:1998/api/locPost', {
            idCity: idCity,
            idDistrict: idDistrict,
            price: price,
            acreage: acreage
        }).then(res => {
            if (res.data.error === false) {
                this.setState({
                    data: res.data.data
                }, () => console.log("data tra ve: ", this.state.data))
            } else {
                alert("loi loi loi !")
            }
        }).catch(err => {
            console.log("fail 222", err);
        })
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 7, backgroundColor: "#FF6600", flexDirection: 'row' }}>
                    <View>
                        <TouchableOpacity
                            onPress={() => {
                                const loadPage = this.props.navigation.getParam('onLoadPage', () => { });
                                loadPage()
                                this.props.navigation.goBack()
                            }}
                        >
                            <Image style={{ height: 30, width: 30, marginTop: 10 }} source={require("../icon/back.png")}>
                            </Image>
                        </TouchableOpacity>

                    </View>
                    <View>
                        <Text style={{
                            color: 'white',
                            fontWeight: 'bold',
                            fontSize: 25,
                            marginTop: 6,
                            marginLeft: 130
                        }}>
                            Bản Tin
                        </Text>
                    </View>
                </View>
                <View style={{ flex: 93, backgroundColor: "white" }}>
                    <FlatList
                        data={this.state.data}
                        renderItem={({ item, index }) => {
                            console.log(`Item = ${JSON.stringify(item)}, Index = ${index}`);
                            return (<FlatListItem item={item} index={index} navigation={this.props.navigation} >

                            </FlatListItem>)
                        }}
                    >

                    </FlatList>
                </View>
            </View>
        );
    }
}
