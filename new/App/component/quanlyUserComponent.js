import React, { Component } from 'react';
import { Image, View, Text, FlatList, Alert } from 'react-native';
import callApi from '../util/apiCaller';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Swipeout from 'react-native-swipeout';
import AddModal1 from './addUser';
import AddModal from './editUser';
const uri = "http://192.168.56.1:1998/image/upload/";

class FlatListItem1 extends Component {
    render() {
        return (
            <View
                style={{
                    margin: 10,
                    flex: 1,
                    borderTopLeftRadius: 20,
                    borderTopRightRadius: 20,
                    borderBottomLeftRadius: 20,
                    borderBottomRightRadius: 20,
                    borderWidth: 1

                }}>

                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View>
                        <Image
                            source={require("../image/2.jpg")}
                            style={{ width: 100, height: 130, borderRadius: 10, marginTop: 10, marginLeft: 10 }}
                        >
                        </Image>
                    </View>

                    <View>
                        <Text style={{ color: 'black', fontWeight: 'bold', marginTop: 15, marginLeft: 50, fontSize: 20 }}>
                            {this.props.item.name}
                        </Text>
                        <Text style={{ color: '#FF6600', marginTop: 5, marginLeft: 20 }}>
                            {this.props.item.email}
                        </Text>
                        <Text style={{ color: '#FF6600', marginTop: 10, marginLeft: 20 }}>
                            {this.props.item.phone}
                        </Text>
                    </View>


                </View>

            </View>
        )
    }
}
class FlatListItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            deletedRowKey: null,
            activeRowKey: null,
        }
    }
    deletePost = async () => {
        const id = this.props.item._id
        console.log("========================================>", id);
        await callApi('DELETE', 'http://192.168.56.1:1998/api/deleteUser', {
            "id": id
        }).then(res => {
            if (res.data.error === false) {
                alert("xoa thành công");
                this.props.parentFlatList.refreshFlatList(id)
            } else {
                alert("loi loi loi !")
            }
        }).catch(err => {
            console.log("fail 222", err);
        })
    }
    render() {
        const { _id } = this.props.item
        console.log("tui can iddddd", _id);
        const SwipeSeting = {
            autoClose: true,
            onClose: (selectId, rowId, direction) => {
                if (this.state.activeRowKey != null) {
                    this.setState({ activeRowKey: null })
                }
            },
            onOpen: (selectId, rowId, direction) => {
                this.setState({ activeRowKey: this.props.item._id },
                    () => {
                        console.log(this.state.activeRowKey)
                    })
            },
            right: [
                // {
                //     onPress: () => {
                //         this.props.parentFlatList.refs.addModal.showAddModal();
                //     },
                //     text: 'Sửa', type: 'primary'
                // },
                {
                    onPress: () => {
                        Alert.alert(
                            'Alert',
                            'Bạn có chắc chắn muốn xóa ?',
                            [
                                { text: "No", onPress: () => console.log('Không xóa'), style: 'cancel' },
                                {
                                    text: 'Yes', onPress: async () => {
                                        this.deletePost();
                                    }
                                }
                            ],
                            { cancelable: true }
                        );
                    },
                    text: 'Xóa', type: 'delete'
                }
            ],
            rowId: this.props.index,
            sectionId: 1
        };
        return (
            <Swipeout {...SwipeSeting}>
                <View
                    style={{
                        margin: 10,
                        flex: 1,
                        borderTopLeftRadius: 20,
                        borderTopRightRadius: 20,
                        borderBottomLeftRadius: 20,
                        borderBottomRightRadius: 20,
                        borderWidth: 1

                    }}>

                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View>
                            <Image
                                source={require("../image/2.jpg")}
                                style={{ width: 100, height: 130, borderRadius: 10, marginTop: 10, marginLeft: 10 }}
                            >
                            </Image>
                        </View>

                        <View>
                            <Text style={{ color: 'black', fontWeight: 'bold', marginTop: 15, marginLeft: 50, fontSize: 20 }}>
                                {this.props.item.name}
                            </Text>
                            <Text style={{ color: '#FF6600', marginTop: 5, marginLeft: 20 }}>
                                {this.props.item.email}
                            </Text>
                            <Text style={{ color: '#FF6600', marginTop: 10, marginLeft: 20 }}>
                                {this.props.item.phone}
                            </Text>
                        </View>


                    </View>

                </View>
            </Swipeout>
        );
    }
}
export default class QuanLyUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFetching: false,
            data1: [],
            data: [],
            refreshing: false,
            deletedRowKey: null,
        };
        this.flatList();
        this.flatList1();
        this.createUser = this.createUser.bind(this);
    }

    refreshFlatList = (deletedKey) => {
        const newData = this.state.data.filter((data, index) => data._id != deletedKey)
        this.setState({ data: newData })
    }

    flatList = async () => {
        this.setState({ isFetching: true })
        await callApi('GET', 'http://192.168.56.1:1998/api/getUserNoAdmin', {
            // "email": email
        }).then(res => {
            if (res.data.error === false) {
                this.setState({
                    data: res.data.data,
                    isFetching: false
                }, () => console.log("data tra ve: ", this.state.data))
            } else {
                alert("loi loi loi !")
            }
        }).catch(err => {
            console.log("fail 222", err);
        })
    }

    flatList1 = async () => {
        this.setState({ isFetching: true })
        await callApi('GET', 'http://192.168.56.1:1998/api/getUserAdmin', {
            // "email": email
        }).then(res => {
            if (res.data.error === false) {
                this.setState({
                    data1: res.data.data,
                    isFetching: false
                }, () => console.log("data tra ve: ", this.state.data))
            } else {
                alert("loi loi loi !")
            }
        }).catch(err => {
            console.log("fail 222", err);
        })
    }
    createUser() {
        this.refs.createModal.showAddModal2()
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 7, backgroundColor: "#FF6600", flexDirection: 'row' }}>
                    <View>
                        <TouchableOpacity
                            onPress={() => {
                                this.props.navigation.navigate('Cánhân')
                            }}
                        >
                            <Image style={{ height: 30, width: 30, marginTop: 10 }} source={require("../icon/back.png")}>
                            </Image>
                        </TouchableOpacity>

                    </View>
                    <View>
                        <Text style={{
                            color: 'white',
                            fontWeight: 'bold',
                            fontSize: 25,
                            marginTop: 6,
                            marginLeft: 45
                        }}>
                            Quản Lý Người Dùng
                        </Text>
                    </View>
                </View>

                <View style={{ flex: 6, flexDirection: 'row' }} >
                    <Text style={{ marginTop: 10, marginLeft: 100, fontWeight: 'bold' }} >Danh sách người dùng</Text>
                    <TouchableOpacity
                        onPress={this.createUser}
                    >
                        <Image style={{ height: 33, width: 33, marginLeft: 70 }} source={require("../icon/add.png")}>
                        </Image>
                    </TouchableOpacity>

                </View>
                <AddModal1 ref={'createModal'} refeshdata={this.refeshdata} parentFlatList={this}>

                </AddModal1>
                <View style={{ flex: 51, backgroundColor: "white" }}>
                    <FlatList
                        onRefresh={() => this.flatList()}
                        refreshing={this.state.isFetching}
                        data={this.state.data}
                        renderItem={({ item, index }) => {
                            console.log(`Item = ${JSON.stringify(item)}, Index = ${index}`);
                            return (<FlatListItem item={item} index={index} parentFlatList={this} navigation={this.props.navigation} >

                            </FlatListItem>)
                        }}
                    >
                    </FlatList>
                    <AddModal ref={'addModal'} updatedata={this.updatedata} parentFlatList={this}>

                    </AddModal>
                </View>

                <View style={{ flex: 6, flexDirection: 'row' }} >
                    <Text style={{ marginTop: 10, marginLeft: 120, fontWeight: 'bold' }} >Danh sách Admin</Text>
                </View>
                <View style={{ flex: 30, backgroundColor: "white" }}>
                    <FlatList
                        onRefresh={() => this.flatList()}
                        refreshing={this.state.isFetching}
                        data={this.state.data1}
                        renderItem={({ item, index }) => {
                            console.log(`Item = ${JSON.stringify(item)}, Index = ${index}`);
                            return (<FlatListItem1 item={item} index={index} parentFlatList={this} navigation={this.props.navigation} >

                            </FlatListItem1>)
                        }}
                    >
                    </FlatList>
                </View>
            </View>
        );
    }
}
