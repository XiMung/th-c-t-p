var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var userRouter = require("./Router/userRouter");
var kindRouter = require("./Router/kindRouter");
var postRouter = require("./Router/postRouter");
var cityRouter = require("./Router/cityRouter");
var houseRouter = require("./Router/houseRouter");


var app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/image', express.static(__dirname + '/public'));

app.use('/api', userRouter);
app.use('/api', kindRouter);
app.use('/api', postRouter);
app.use('/api', cityRouter);
app.use('/api', houseRouter);

var connect = mongoose.connect("mongodb://localhost:27017/AppHouseLand");
// var connect = mongoose.connect("mongodb+srv://leanh:anh0944164009@cluster0-fsymw.mongodb.net/ALTP?retryWrites=true&w=majority", 
// {   useNewUrlParser: true,
//     useUnifiedTopology: true,
//     useCreateIndex: true });

app.listen(process.env.PORT || 1998, function () {
    console.log("ung dung chay tren port 1998");
});