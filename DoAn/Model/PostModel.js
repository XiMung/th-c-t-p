var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var eventSchema = new Schema({
    title: String,
    content: String,
    address: String,
    verify: Number,
    image: String,
    idUser: { type: Schema.ObjectId, ref: "user" },
    idKind: { type: Schema.ObjectId, ref: "kind" }
});

var postModel = mongoose.model("post", eventSchema);
module.exports = {
    postModel: postModel
}