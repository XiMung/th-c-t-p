var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var houseSchema = Schema({
    acreage: String,
    price: Number,
    image: [String],
    floor: Number,
    bedroom: Number,
    bathroom: Number,
    legal: String,
    balcony: Number,
    terrace: Number,
    note: String,
    placeholder: Number,
    idPost: { type: Schema.ObjectId, ref: "post" }
});

var houseModel = mongoose.model("house", houseSchema);
module.exports = {
    houseModel: houseModel
}