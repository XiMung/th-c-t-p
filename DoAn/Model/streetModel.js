var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var streetSchema = Schema({
    name: String,
    idDistrict: { type: Schema.ObjectId, ref: "district" }
});

var streetModel = mongoose.model("street", streetSchema);
module.exports = {
    streetModel: streetModel
}