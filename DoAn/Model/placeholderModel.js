var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var placeholderSchema = Schema({
    idUser: { user: Schema.ObjectId, ref: "user" },
    idHouse: { house: Schema.ObjectId, ref: "house" }
});

var placeholderModel = mongoose.model("placeholder", placeholderSchema);
module.exports = {
    placeholderModel: placeholderModel
}