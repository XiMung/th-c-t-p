var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var UserSchema = Schema({
    email: String,
    password: String,
    name: String,
    phone: String,
    avatar: String,
    address: String,
    role: Number
});

var userModel = mongoose.model("user", UserSchema);
module.exports = {
    userModel: userModel
}