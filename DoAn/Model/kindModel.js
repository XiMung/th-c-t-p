var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var kindSchema = Schema({
    name: String
});

var kindModel = mongoose.model("kind", kindSchema);
module.exports = {
    kindModel: kindModel
}