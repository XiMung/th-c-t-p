var router = require("express").Router();
var { postModel } = require("../Model/PostModel");
var { upload } = require("../middleware/multer.middleware");


router.post("/createPost", upload.single('image'), createPost);
router.patch("/updatePost", upload.single('image'), updatePost);
router.get("/getPost", getPost);
router.delete("/deletePost", deletePost);
router.delete("/getPostVerify1", getPostVerify1);
router.delete("/getPostVerify0", getPostVerify0);

module.exports = router;


// them  ban nha
async function createPost(req, res) {
    var title = req.body.title;
    if (title == null || title == "" || title == undefined) {
        res.json({
            statusCode: 400,
            message: "loi title"
        })
        return;
    }
    var content = req.body.content;
    if (content == null || content == "" || content == undefined) {
        res.json({
            statusCode: 400,
            message: "loi content"
        })
        return;
    }
    var address = req.body.address;
    if (address == null || address == "" || address == undefined) {
        res.json({
            statusCode: 400,
            message: "loi address"
        })
        return;
    }
    var verify = req.body.verify;
    if (verify == null || verify == "" || verify == undefined) {
        res.json({
            statusCode: 400,
            message: "loi verify"
        })
        return;
    }
    var image = req.file;
    if (image == null || image == "" || image == undefined) {
        res.json({
            statusCode: 400,
            message: "loi image"
        })
        return;
    }
    var idUser = req.body.idUser;
    if (idUser == null || idUser == "" || idUser == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idUser"
        })
        return;
    }
    var idKind = req.body.idKind;
    if (idKind == null || idKind == "" || idKind == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idKind"
        })
        return;
    }

    var post = new postModel({
        title: title,
        content: content,
        address: address,
        verify: verify,
        image: image.filename,
        idUser: idUser,
        idKind: idKind
    });
    var savePost = await post.save();
    if (savePost) {
        return res.json({ error: false, data: savePost });
    }
    else {
        return res.json({ error: true, message: 'Them that bai' });
    }
}
// lay danh sach post
async function getPost(req, res) {
    let data = await postModel.find({});
    if (data) {
        return res.json({ error: false, data: data });
    } else {
        return res.json({ error: true, message: 'lay danh sach that bai' });
    }
}
// lay danh sach theo verify 1
async function getPostVerify1(req, res) {
    let data = await postModel.find({ verify: 1 });
    if (data) {
        return res.json({ error: false, data: data });
    } else {
        return res.json({ error: true, message: 'lay danh sach that bai' });
    }
}
// lay danh sach theo verify 0
async function getPostVerify0(req, res) {
    let data = await postModel.find({ verify: 0 });
    if (data) {
        return res.json({ error: false, data: data });
    } else {
        return res.json({ error: true, message: 'lay danh sach that bai' });
    }
}

// sua sale apartment
async function updatePost(req, res) {
    var id = req.body.id;
    var title = req.body.title;
    var content = req.body.content;
    var address = req.body.address;
    var verify = req.body.verify;
    var image = req.body.image;
    var idKind = req.body.idKind;

    let result = await saleApartmentModel.findOne({ _id: id });
    if (title == null || title == "" || title == undefined) {
        title = result.title;
    }
    if (content == null || content == "" || content == undefined) {
        content = result.content;
    }
    if (address == null || address == "" || address == undefined) {
        address = result.address;
    }
    if (verify == null || verify == "" || verify == undefined) {
        verify = result.verify;
    }
    if (image == null || image == "" || image == undefined) {
        image = result.image;
    }
    if (idKind == null || idKind == "" || idKind == undefined) {
        idKind = result.idKind;
    }

    let data = await postModel.updateOne({ _id: id },
        {
            title: title, content: content, address: address, verify: verify, image: image.filename, idKind: idKind
        });
    if (data) {
        return res.json({ err: false, data: data })
    } else {
        return res.json({ err: true, message: "loi update" })
    }
}

// xoa sale apartment
async function deletePost(req, res) {
    var id = req.body.id;
    if (id == null || id == "" || id == undefined) {
        res.json({
            statusCode: 400,
            message: "loi xoa"
        })
        return;
    }
    let data = await postModel.deleteOne({ _id: id });
    if (data) {
        return res.json({ error: false, data: data });
    } else {
        return res.json({ error: true, message: 'loi xoa' })
    }
}