var router = require("express").Router();
var { kindModel } = require("../Model/kindModel");

router.post("/createKind", createKind);
router.delete("/deleteKind", deleteKind);
router.patch("/updateKind", updateKind);
router.get("/getKind", getKind);

module.exports = router;

// them kind
async function createKind(req, res) {
    var name = req.body.name;
    if (name == null || name == "" || name == undefined) {
        res.json({
            statusCode: 400,
            message: "loi name"
        })
        return;
    }

    var kind = new kindModel({
        name: name
    });
    var saveKind = await kind.save();
    if (saveKind) {
        return res.json({ error: false, data: saveKind });
    }
    else {
        return res.json({ error: true, message: 'Them that bai' });
    }
}

// lấy tất cả kind
async function getKind(req, res) {
    var data = await kindModel.find({});
    if (data) {
        return res.json({ error: false, data: data });
    }
    else {
        return res.json({ error: true, message: 'hehe' });
    }
}


// // sua kind
async function updateKind(req, res) {
    var id = req.body.id;
    var name = req.body.name;

    let result = await userModel.findOne({ _id: id });
    if (name == null || name == "" || name == undefined) {
        name = result.name;
    }
    let data = await kindModel.updateOne({ _id: id },
        { name: name });
    if (data) {
        return res.json({ err: false, data: data })
    } else {
        return res.json({ err: true, message: "loi update" })
    }
}

// xoa kind
async function deleteKind(req, res) {
    var id = req.body.id;
    if (id == null || id == "" || id == undefined) {
        res.json({
            statusCode: 400,
            message: "loi xoa"
        })
        return;
    }
    let data = await userModel.deleteOne({ _id: id });
    if (data) {
        return res.json({ error: false, data: data });
    } else {
        return res.json({ error: true, message: 'loi xoa' })
    }
}