var router = require("express").Router();
var { houseModel } = require("../Model/houseModel");
var { upload } = require("../middleware/multer.middleware");


router.post("/createHouse", upload.array('image', 10), createHouse);
router.patch("/updateHouse", updateHouse);
router.get("/getHouse", getHouse);
router.delete("/deleteHouse", deleteHouse);
router.get("/getHousePost", getHousePost);


module.exports = router;


// them  
async function createHouse(req, res) {
    var acreage = req.body.acreage;
    if (acreage == null || acreage == "" || acreage == undefined) {
        res.json({
            statusCode: 400,
            message: "loi acreage"
        })
        return;
    }
    var price = req.body.price;
    if (price == null || price == "" || price == undefined) {
        res.json({
            statusCode: 400,
            message: "loi price"
        })
        return;
    }
    var image = req.files;
    if (image == null || image == "" || image == undefined) {
        res.json({
            statusCode: 400,
            message: ""
        })
        return;
    }
    var floor = req.body.floor;
    if (floor == null || floor == "" || floor == undefined) {
        res.json({
            statusCode: 400,
            message: "loi floor"
        })
        return;
    }
    var bedroom = req.body.bedroom;
    if (bedroom == null || bedroom == "" || bedroom == undefined) {
        res.json({
            statusCode: 400,
            message: "loi bedroom"
        })
        return;
    }
    var bathroom = req.body.bathroom;
    if (bathroom == null || bathroom == "" || bathroom == undefined) {
        res.json({
            statusCode: 400,
            message: "loi bathroom"
        })
        return;
    }
    var legal = req.body.legal;
    if (legal == null || legal == "" || legal == undefined) {
        res.json({
            statusCode: 400,
            message: "loi legal"
        })
        return;
    }
    var balcony = req.body.balcony;
    if (balcony == null || balcony == "" || balcony == undefined) {
        res.json({
            statusCode: 400,
            message: "loi balcony"
        })
        return;
    }
    var terrace = req.body.terrace;
    if (terrace == null || terrace == "" || terrace == undefined) {
        res.json({
            statusCode: 400,
            message: "loi terrace"
        })
        return;
    }
    var note = req.body.note;
    if (note == null || note == "" || note == undefined) {
        res.json({
            statusCode: 400,
            message: "loi note"
        })
        return;
    }
    var placeholder = req.body.placeholder;
    if (placeholder == null || placeholder == "" || placeholder == undefined) {
        res.json({
            statusCode: 400,
            message: "loi placeholder"
        })
        return;
    }
    var idPost = req.body.idPost;
    if (idPost == null || idPost == "" || idPost == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idPost"
        })
        return;
    }

    var A = [];
    for (let i = 0; i < image.length; i++) {
        A[i] = image[i].filename;
    }

    var land = new houseModel({
        acreage: acreage,
        price: price,
        image: A,
        floor: floor,
        bedroom: bedroom,
        bathroom: bathroom,
        legal: legal,
        balcony: balcony,
        terrace: terrace,
        note: note,
        placeholder: placeholder,
        idPost: idPost
    });
    var saveLand = await land.save();
    if (saveLand) {
        return res.json({ error: false, data: saveLand });
    }
    else {
        return res.json({ error: true, message: 'Them that bai' });
    }
}
// lấy house theo post
async function getHousePost(req, res) {
    var idPost = req.body.idPost;
    if (idPost == null || idPost == "" || idPost == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idPost"
        })
        return;
    }
    let data = await houseModel.find({ idPost: idPost });
    if (data) {
        return res.json({ error: false, data: data });
    } else {
        return res.json({ error: true, message: 'lay danh sach that bai' });
    }
}
// lay tất cả house 
async function getHouse(req, res) {
    let data = await houseModel.find({});
    if (data) {
        return res.json({ error: false, data: data });
    } else {
        return res.json({ error: true, message: 'lay danh sach that bai' });
    }
}

// // sua  houser
async function updateHouse(req, res) {
    var id = req.body.id;
    var acreage = req.body.acreage;
    var price = req.body.price;
    var floor = req.body.floor;
    var bedroom = req.body.bedroom;
    var bathroom = req.body.bathroom;
    var legal = req.body.legal;
    var balcony = req.body.balcony;
    var terrace = req.body.terrace;
    var note = req.body.note;
    var placeholder = req.body.placeholder;

    let result = await houseModel.findOne({ _id: id });
    if (acreage == null || acreage == "" || acreage == undefined) {
        acreage = result.acreage;
    }
    if (price == null || price == "" || price == undefined) {
        price = result.price;
    }
    if (floor == null || floor == "" || floor == undefined) {
        floor = result.floor;
    }
    if (bedroom == null || bedroom == "" || bedroom == undefined) {
        bedroom = result.bedroom;
    }
    if (bathroom == null || bathroom == "" || bathroom == undefined) {
        bathroom = result.bathroom;
    }
    if (legal == null || legal == "" || legal == undefined) {
        legal = result.legal;
    }
    if (balcony == null || balcony == "" || balcony == undefined) {
        balcony = result.balcony;
    }
    if (terrace == null || terrace == "" || terrace == undefined) {
        terrace = result.terrace;
    }
    if (note == null || note == "" || note == undefined) {
        note = result.note;
    }
    if (placeholder == null || placeholder == "" || placeholder == undefined) {
        placeholder = result.placeholder;
    }

    let data = await houseModel.updateOne({ _id: id },
        {
            acreage: acreage, floor: floor, bedroom: bedroom, price: price, placeholder: placeholder,
            bathroom: bathroom, balcony: balcony, terrace: terrace, note: note, legal: legal
        });
    if (data) {
        return res.json({ err: false, data: data })
    } else {
        return res.json({ err: true, message: "loi update" })
    }
}

// xoa sale land
async function deleteHouse(req, res) {
    var id = req.body.id;
    if (id == null || id == "" || id == undefined) {
        res.json({
            statusCode: 400,
            message: "loi xoa"
        })
        return;
    }
    let data = await houseModel.deleteOne({ _id: id });
    if (data) {
        return res.json({ error: false, data: data });
    } else {
        return res.json({ error: true, message: 'loi xoa' })
    }
}