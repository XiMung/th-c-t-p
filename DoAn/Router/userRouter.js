var router = require("express").Router();
var { userModel } = require("../Model/userModel");
var { upload } = require("../middleware/multer.middleware");
var crypto = require("crypto");
var cons = require("../cons");


router.post("/createUser", upload.single('avatar'), createUser);
router.patch("/updateUser", upload.single('avatar'), updateUser);
router.get("/getUser", getUser);
router.delete("/deleteUser", deleteUser);
router.post("/login", login);


module.exports = router;

async function createUser(req, res) {
    var email = req.body.email;
    if (!email) {
        res.json({
            statuscode: 400,
            message: "bạn chưa nhập email"
        });
        return;
    }
    if (!cons.paterEmail.test(email)) {
        res.json({
            statuscode: 400,
            message: "cú pháp mail của bạn không hợp lệ"
        });
        return;
    }

    var password = req.body.password;
    if (!password) {
        res.json({
            statuscode: 400,
            message: "bạn chưa nhập password"
        });
        return;
    }
    if (password.length < 4 || password.length > 15) {
        res.json({
            statuscode: 400,
            message: "password phải từ 5 đến 15 ký tự"
        });
        return;
    }

    var name = req.body.name;
    if (!name) {
        res.json({
            statuscode: 400,
            message: "bạn chưa nhập name"
        });
        return;
    }

    var phone = req.body.phone;
    if (!phone) {
        res.json({
            statuscode: 400,
            message: "bạn chưa nhập phone"
        });
        return;
    }

    var avatar = req.file;
    if (avatar == null || avatar == "" || avatar == undefined) {
        res.json({
            statusCode: 400,
            message: "loi avatar"
        })
        return;
    }
    var address = req.body.address;
    if (address == null || address == "" || address == undefined) {
        res.json({
            statusCode: 400,
            message: "loi address"
        })
        return;
    }

    let data = await userModel.find({ email: email });

    if (data.length > 0) {
        res.json({
            statusCode: 400,
            message: "email đã tồn tại"
        })
        return;
    }
    var hash = await crypto.createHmac('sha256', "ALTP")
        .update(password)
        .digest('hex');
    password = hash;

    var user = new userModel({
        email: email,
        password: password,
        name: name,
        phone: phone,
        avatar: avatar.filename,
        address: address,
        role: 1
    });
    var saveUser = await user.save();
    if (saveUser) {
        return res.json({ error: false, data: saveUser });
    }
    else {
        return res.json({ error: true, message: 'Them that bai' });
    }
}

// lấy tất cả các user
async function getUser(req, res) {
    var data = await userModel.find({});
    if (data) {
        return res.json({ error: false, data: data });
    }
    else {
        return res.json({ error: true, message: 'hehe' });
    }
}


// // sua user
async function updateUser(req, res) {
    var id = req.body.id;
    var email = req.body.email;
    var name = req.body.name;
    var phone = req.body.phone;
    var address = req.body.address;
    var avatar = req.file;
    var role = req.body.role;

    let result = await userModel.findOne({ _id: id });
    if (email == null || email == "" || email == undefined) {
        email = result.email;
    }
    if (name == null || name == "" || name == undefined) {
        name = result.name;
    }
    if (phone == null || phone == "" || phone == undefined) {
        phone = result.phone;
    }
    if (address == null || address == "" || address == undefined) {
        address = result.address;
    }
    if (avatar == null || avatar == "" || avatar == undefined) {
        avatar = result.avatar;
    }
    if (role == null || role == "" || role == undefined) {
        role = result.role;
    }

    let data = await userModel.updateOne({ _id: id },
        { email: email, name: name, phone: phone, address: address, avatar: avatar.filename, role: role });
    if (data) {
        return res.json({ err: false, data: data })
    } else {
        return res.json({ err: true, message: "loi update" })
    }
}

// xoa user
async function deleteUser(req, res) {
    var id = req.body.id;
    if (id == null || id == "" || id == undefined) {
        res.json({
            statusCode: 400,
            message: "loi xoa"
        })
        return;
    }
    let data = await userModel.deleteOne({ _id: id });
    if (data) {
        return res.json({ error: false, data: data });
    } else {
        return res.json({ error: true, message: 'loi xoa' })
    }
}

// đăng nhập
async function login(req, res) {
    var email = req.body.email;
    if (!email) {
        res.json({
            statuscode: 400,
            message: "bạn chưa nhập username"
        });
        return;
    }
    if (!cons.paterEmail.test(email)) {
        res.json({
            statuscode: 400,
            message: "cú pháp mail của bạn không hợp lệ"
        });
        return;
    }

    var password = req.body.password;
    if (!password) {
        res.json({
            statuscode: 400,
            message: "bạn chưa nhập password"
        });
        return;
    }

    let data = await userModel.find({ email: email });
    if (data.length > 0) {
        var hash = crypto.createHmac('sha256', "ALTP")
            .update(password)
            .digest('hex');

        if (hash == data[0].password) {
            return res.json({ error: false, message: 'đăng nhập thành công' });
        } else {
            return res.json({ error: true, message: 'sai password đăng nhập' });
        }
    } else {
        return res.json({ error: true, message: 'sai email đăng nhập' });
    }
}